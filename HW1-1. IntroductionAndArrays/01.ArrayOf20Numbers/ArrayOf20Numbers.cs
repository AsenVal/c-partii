﻿using System;

class ArrayOf20Numbers
{
    static void Main()
    {
        int[] Numberarray = new int[20];
        for (int i = 0; i < 20; i++)
        {
            Numberarray[i] = i * 5;
            Console.WriteLine(Numberarray[i]);
        }
    }
}
