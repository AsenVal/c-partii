﻿using System;

class CompareArraysElementByElement
{
    static void Main()
    {
        Console.WriteLine("Insert first array and separate its elements with ;");
        string FirstRow = Console.ReadLine();
        Console.WriteLine("Insert second array and separate its elements with ;");
        string SecondRow = Console.ReadLine();
        int[] FirstArray = new int[50];
        int[] SecondArray = new int[50];
        string str="";
        int count = 0;
        for (int i = 0; i < FirstRow.Length; i++)
        {
            str += FirstRow.Substring(i, 1);
            if (FirstRow.Substring(i, 1) == ";")
            {
                FirstArray[count] = Convert.ToInt32(str.Substring(0, str.Length -1));
                count++;
                str = "";
            }
        }
        if (str != "")
            FirstArray[count] = Convert.ToInt32(str);

        str = "";
        count = 0;
        for (int i = 0; i < SecondRow.Length; i++)
        {
            str += SecondRow.Substring(i, 1);
            if (SecondRow.Substring(i, 1) == ";")
            {
                SecondArray[count] = Convert.ToInt32(str.Substring(0, str.Length - 1));
                count++;
                str = "";
            }
        }
        if(str != "")
            SecondArray[count] = Convert.ToInt32(str);

        bool Istrue = true;
        if (FirstArray.Length == SecondArray.Length)
        {
            for (int i = 0; i < FirstArray.Length; i++)
            {
                if (FirstArray[i] != SecondArray[i])
                {
                    Istrue = false;
                    break;
                }
            }
            if (Istrue == true)
                Console.WriteLine("Arrays are equal");
            else
                Console.WriteLine("Arrays are not equal");
        }
        else
            Console.WriteLine("Arrays are not equal");
    }
}