﻿using System;

class CompareTwoCharArrays
{
    static void Main()
    {
        Console.WriteLine("Insert first string");
        string FirstRow = Console.ReadLine();
        Console.WriteLine("Insert second string");
        string SecondRow = Console.ReadLine();
        string Cleter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string Sleter = "abcdefghijklmnopqrstuvwxyz";
        bool IsEqual = true;
        for (int i = 0; i < Math.Max(FirstRow.Length, SecondRow.Length); i++)
        {
            if (FirstRow.Substring(i, 1) != SecondRow.Substring(i, 1))
            {
                int resultF = Cleter.IndexOf(FirstRow.Substring(i, 1));
                int resultS = Cleter.IndexOf(SecondRow.Substring(i, 1));
                if (resultF == -1)
                {
                    resultF = Sleter.IndexOf(FirstRow.Substring(i, 1));
                }
                if (resultS == -1)
                {
                    resultS = Sleter.IndexOf(SecondRow.Substring(i, 1));
                }
                if (resultF < resultS)
                {
                    Console.WriteLine("First string is lexicographically smaller");
                    IsEqual = false;
                    break;
                }
                else
                {
                    Console.WriteLine("Second string is lexicographically smaller");
                    IsEqual = false;
                    break;
                }

            }
            else if (i == FirstRow.Length - 1 && i != SecondRow.Length - 1)
            {
                Console.WriteLine("First string is lexicographically smaller");
                IsEqual = false;
                break;
            }
            else if (i != FirstRow.Length - 1 && i == SecondRow.Length - 1)
            {
                Console.WriteLine("Second string is lexicographically smaller");
                IsEqual = false;
                break;
            }
            else
                IsEqual = true;
        }
        if (IsEqual == true)
            Console.WriteLine("Two strings are lexicographically equal");
        
    }
}
