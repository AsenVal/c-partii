﻿using System;

class MaximalSequenceOfEqualElements
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");        
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }

        int Element = FirstArray[0];
        int MaxElement = Element;
        int Maxcount = 0;
        int count = 0;
        for (int i = 1; i < FirstArray.Length; i++)
        {
            if (FirstArray[i] == Element)
            {
                count++;
                if (count > Maxcount)
                {
                    Maxcount = count;
                    MaxElement = Element;
                }
            }
            else
            {
                Element = FirstArray[i];
                count = 0;
            }
        }

        Console.Write("{");
        Console.Write(MaxElement);
        for (int i = 0; i < Maxcount; i++)
        {
            Console.Write(",{0}",MaxElement);
        }
        Console.WriteLine("}");

    }
}
