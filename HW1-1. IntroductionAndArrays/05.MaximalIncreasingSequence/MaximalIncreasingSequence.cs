﻿using System;

class MaximalIncreasingSequence
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }
                
        int Maxcount = 0;
        int count = 0;
        int positionElement = 0;
        for (int i = 0; i < FirstArray.Length - 1; i++)
        {
            if (FirstArray[i] < FirstArray[i+1])
            {
                count++;
                if (count > Maxcount)
                {
                    positionElement = i+1;
                    Maxcount = count;
                }
            }
            else
            {
                count = 0;
            }
        }

        Console.Write("{");
        Console.Write(FirstArray[positionElement-Maxcount]);
        for (int i = positionElement - Maxcount + 1; i < positionElement+1; i++)
        {
            Console.Write(",{0}", FirstArray[i]);
        }
        Console.WriteLine("}");
    }
}
