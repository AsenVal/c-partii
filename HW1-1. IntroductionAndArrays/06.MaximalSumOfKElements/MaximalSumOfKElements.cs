﻿using System;

class MaximalSumOfKElements
{
    static void Main()
    {
        Console.WriteLine("Insert N:");
        int N = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert K:");
        int K = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[N];
        for (int i = 0; i < N; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }
        int Sum = FirstArray[0] + FirstArray[1] + FirstArray[2];
        int MaxSum = Sum;
        for (int i = 2; i < FirstArray.Length - 1; i++)
        {
            Sum += FirstArray[i + 1];
            Sum -= FirstArray[i - 2];
            if (Sum > MaxSum)
            {
                MaxSum = Sum;
            }
        }
        Console.WriteLine("Max sum is {0}", MaxSum);

    }
}
