﻿using System;

class SelectionSortArray
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }

        // Sorting array with "selection sort" algorithm
        int minElement;
        for (int i = 0; i < FirstArray.Length; i++)
        {
            minElement = FirstArray[i];
            for (int j = i; j < FirstArray.Length; j++)
            {
                if (FirstArray[j] < minElement)
                {
                    minElement = FirstArray[j];
                    FirstArray[j] = FirstArray[i];
                    FirstArray[i] = minElement;                    
                }
            }
        }
        Console.Write("{");
        Console.Write("{0}", FirstArray[0]);
        for (int i = 1; i < FirstArray.Length; i++)
        {
            Console.Write(",{0}",FirstArray[i]);            
        }
        Console.WriteLine("}");

    }
}
