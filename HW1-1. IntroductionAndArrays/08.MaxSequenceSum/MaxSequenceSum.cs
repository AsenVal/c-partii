﻿using System;

class MaxSequenceSum
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }

        int Sum = FirstArray[0];
        int MaxSum = Sum;
        int firstElement = 0;
        int lastElement = 0;
        int maxfirstElement = 0;
        int maxlastElement = 0;
        for (int i = 1; i < FirstArray.Length; i++)
        {
            if (Sum + FirstArray[i] > 0)
            {
                Sum += FirstArray[i];
                lastElement = i;
            }
            else
            {
                Sum = 0;
                firstElement = i + 1;
                lastElement = i + 1;
            }

            if (Sum > MaxSum)
            {
                MaxSum = Sum;
                maxfirstElement = firstElement;
                maxlastElement = lastElement;
            }        
        }

        Console.Write("{");
        Console.Write("{0}", FirstArray[firstElement]);
        for (int i = firstElement + 1; i <= maxlastElement; i++)
        {
            Console.Write(",{0}", FirstArray[i]);
        }
        Console.WriteLine("}");
    }
}
