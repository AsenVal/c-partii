﻿using System;
using System.Linq;

class MostFrequenNumber
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }

        ////////
        int[,] Number = new int[n, 2];
        bool IsAdded = false;
        for (int i = 0; i < FirstArray.Length; i++)
        {
            int j = 0;
            do
            {
                if (Number[j, 0] == FirstArray[i])
                {
                    Number[j, 1]++;
                    IsAdded = true;
                }
                j++;
            }
            while (j < n && (Number[j - 1, 0] != 0 || (Number[j - 1, 1] > 0 && IsAdded == false)));
            if(IsAdded == false)
            {
                Number[j - 1, 0] = FirstArray[i];
                Number[j - 1, 1] = 1;
            }
            IsAdded = false;
        }
        int maxNumber = Number[0,0];
        int maxcount = Number[0,1];
        for (int i = 1; i < Number.GetLength(0); i++)
        {
            if (Number[i, 1] > maxcount)
            {
                maxNumber = Number[i, 0];
                maxcount = Number[i, 1];
            }
        }

        Console.WriteLine("{0} ({1} times)", maxNumber, maxcount);

        //// mnogo izchisteno reshenie za tazi zadacha

        /*int[] arr2 = new int[FirstArray.Max() + 1];
        for (int i = 0; i < n; i++)
        {
            arr2[FirstArray[i]]++;
        }
        Console.WriteLine("Maximum times appearing: {0}", arr2.Max());
        Console.WriteLine("The number appearing: {0}", Array.IndexOf(arr2, arr2.Max()));*/
    }
}
