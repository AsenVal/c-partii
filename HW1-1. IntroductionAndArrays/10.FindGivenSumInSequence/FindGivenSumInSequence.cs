﻿using System;

class FindGivenSumInSequence
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }
        Console.WriteLine("Insert given sum");
        int givenSum = int.Parse(Console.ReadLine());

        int Sum = 0;
        bool SumFound = false;
        int firstElement = 0;
        int lastElement = 0;

        for (int i = 0; i < FirstArray.Length; i++)
        {
            Sum = FirstArray[i];
            firstElement = i;
            for (int j = i+1; j < FirstArray.Length; j++)
            {
                if (Sum < givenSum)
                {
                    Sum += FirstArray[j];
                }
                else if (Sum == givenSum)
                {
                    SumFound = true;
                    lastElement = j;
                    break;
                }
                else
                {
                    break;
                }
            }
            if (SumFound == true)
            {
                break;
            }
            else if (Sum == givenSum)
            {
                lastElement = FirstArray.Length;
                break;
            }
        }

        Console.Write("{");
        Console.Write("{0}", FirstArray[firstElement]);
        for (int i = firstElement + 1; i < lastElement; i++)
        {
            Console.Write(",{0}", FirstArray[i]);
        }
        Console.WriteLine("}");
    }
}
