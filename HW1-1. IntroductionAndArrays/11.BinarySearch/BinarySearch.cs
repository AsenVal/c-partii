﻿using System;

class BinarySearch
{
    static void Main()
    {
        int[] ArrangeArray = new int[]{2,5,6,8,9,12,15,18,22,26,39,48};
        int number = 6;

        //Console.WriteLine(Array.BinarySearch(ArrangeArray, number));   direktna funkciq ili si q pishem
        Console.WriteLine(BinSearch(ArrangeArray, number));
    }
    static int BinSearch(int[] array, int number)
    {
        Array.Sort(array);
        int MaxElement = array.Length - 1;
        int MinElement = 0;
        while (MaxElement >= MinElement)
        {
            int Midpoint = (MinElement + MaxElement) / 2;
            if (array[Midpoint] < number)
            {
                MinElement = Midpoint + 1;
            }
            else if (array[Midpoint] > number)
            {
                MaxElement = Midpoint - 1;
            }
            else
            {
                return Midpoint;
            }
        }
        return -1;
    }
}
