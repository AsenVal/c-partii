﻿using System;
using System.Collections.Generic;

class PrintNumbersOfLetters
{
    static void Main()
    {
        char[] Letters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
        Console.WriteLine("Write a word with small letters");
        string str = Console.ReadLine();
        List<int> indexes = new List<int>();
        for (int i = 0; i < str.Length; i++)
        {
            indexes.Add(Array.BinarySearch(Letters,Convert.ToChar(str.Substring(i,1))));
            Console.WriteLine("{0} - {1} index in array",str.Substring(i,1),indexes[i]);
        }

    }
}
