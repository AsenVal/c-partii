﻿using System;

class MergeSortAlgoritam
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] InputArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            InputArray[i] = int.Parse(Console.ReadLine());
        }
        
        /////// Merge Sort
        int[] sortedArray = MergeSort(InputArray);

        // print
        Console.Write("{");
        Console.Write(sortedArray[0]);
        for (int i = 1; i < sortedArray.Length; i++)
        {
            Console.Write(",{0}",sortedArray[i]);
        }
        Console.WriteLine("}");
        
    }
    static int[] MergeSort(int[] a)
    {
        if (a.Length <= 1)
            return a;
        int middle = a.Length / 2;
        int[] left = new int[middle];
        for (int i = 0; i < middle; i++)
        {
            left[i] = a[i];
        }
        int[] right = new int[a.Length - middle];
        for (int i = 0; i < a.Length - middle; i++)
        {
            right[i] = a[i + middle];
        }
        left = MergeSort(left);
        right = MergeSort(right);

        int leftptr = 0;
        int rightptr = 0;

        int[] sorted = new int[a.Length];
        for (int k = 0; k < a.Length; k++)
        {
            if (rightptr == right.Length || ((leftptr < left.Length) && (left[leftptr] <= right[rightptr])))
            {
                sorted[k] = left[leftptr];
                leftptr++;
            }
            else if (leftptr == left.Length || ((rightptr < right.Length) && (right[rightptr] <= left[leftptr])))
            {
                sorted[k] = right[rightptr];
                rightptr++;
            }
        }
        return sorted;
    }
}
