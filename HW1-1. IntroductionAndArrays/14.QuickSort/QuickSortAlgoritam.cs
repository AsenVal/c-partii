﻿using System;

class QuickSortAlgoritam
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] InputArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            InputArray[i] = int.Parse(Console.ReadLine());
        }

        /////// Quick Sort
        QuickSort(InputArray, 0, n - 1);

        // print
        Console.Write("{");
        Console.Write(InputArray[0]);
        for (int i = 1; i < InputArray.Length; i++)
        {
            Console.Write(",{0}", InputArray[i]);
        }
        Console.WriteLine("}");
    }
    public static void QuickSort(int[] elements, int left, int right)
    {
        int i = left, j = right;
        int middle = elements[(left + right) / 2];

        // if you want to see whole process
        //for (int k = 0; k < elements.Length; k++)
        //{
        //    Console.Write(elements[k] + " ");
        //}
        //Console.Write("\"{0}\"", middle);
        //Console.WriteLine();

        while (i <= j)
        {
            while (elements[i].CompareTo(middle) < 0)
            {
                i++;
            }

            while (elements[j].CompareTo(middle) > 0)
            {
                j--;
            }

            if (i <= j)
            {
                int tmp = elements[i];
                elements[i] = elements[j];
                elements[j] = tmp;

                i++;
                j--;
            }
        }

        // Recursive calls
        if (left < j)
        {
            QuickSort(elements, left, j);
        }

        if (i < right)
        {
            QuickSort(elements, i, right);
        }
    }
}
