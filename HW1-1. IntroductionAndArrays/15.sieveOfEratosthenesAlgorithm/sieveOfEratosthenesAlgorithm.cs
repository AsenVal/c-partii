﻿using System;

class sieveOfEratosthenesAlgorithm
{
    static void Main()
    {
        bool[] NumbersArray = new bool[10000000];
        for (int i = 2; i < NumbersArray.Length; i++)
        {
            if (NumbersArray[i] == false)
            {
                for (int j = i * i; j < NumbersArray.Length; j = j + i)
                {
                    NumbersArray[j] = true;
                }
                Console.Write("{0} ", i);
            }
        }
        Console.WriteLine();
    }
}
