﻿using System;
using System.Collections.Generic;

class SubsetOfElementsWithSumS
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }
        Console.WriteLine("Insert number S for desired sum");
        int S = int.Parse(Console.ReadLine());
        bool[] SumArray = new bool[S + 1];
        string[] IndexArray = new string[S + 1];
        int Sum = 0;

        for (int i = 0; i < FirstArray.Length; i++)
        {
            Sum = FirstArray[i];
            if (Sum < SumArray.Length)
            {
                SumArray[Sum] = true;
                IndexArray[Sum] = i.ToString();
            }

            for (int j = SumArray.Length - 1; j > 0; j--)
            {
                if (SumArray[j] == true && j != FirstArray[i])
                {
                    if (Sum + j < SumArray.Length)
                    {
                        SumArray[Sum + j] = true;
                        if (IndexArray[j] != i.ToString())
                        {
                            IndexArray[Sum + j] = IndexArray[j];
                        }
                        IndexArray[Sum + j] = IndexArray[Sum + j] + ";" + i.ToString();
                    }
                }
            }

            if (SumArray[S] == true)
            {
                break;
            }
        }
        if (SumArray[S] == true)
        {
            Console.Write("Yes (");
            string str = "";
            for (int i = 0; i < IndexArray[S].Length; i++)
            {
                //0; 1; 12; 15 print these elements of FirstArray
                if (IndexArray[S].Substring(i, 1) != ";")
                {
                    str = IndexArray[S].Substring(i, 1);
                }
                else
                {
                    Console.Write("{0}+", FirstArray[Convert.ToInt32(str)]);
                    str = "";
                }
            }
            if (str != "")
            {
                Console.Write("{0}", FirstArray[Convert.ToInt32(str)]);
            }
            Console.WriteLine(")");
        }
        else
        {
            Console.WriteLine("No");
        }

        /*Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }
        Console.WriteLine("Insert number S for desired sum");
        int S = int.Parse(Console.ReadLine());
        bool[] SumArray = new bool[S + 1];
        List<int>[] IndexArray = new List<int>[S + 1];  // Define of Array with List elements for Subsums

        // Inicializing of the array of Lists
        for (int i = 0; i < S + 1; i++)
        {
            IndexArray[i] = new List<int>();
        }
        int Sum = 0;

        for (int i = 0; i < FirstArray.Length; i++)
        {         
            Sum = FirstArray[i];
            if (Sum < SumArray.Length)
            {
                SumArray[Sum] = true;
                if (IndexArray[Sum].Count != 0)
                {
                    IndexArray[Sum].Clear();
                }
                IndexArray[Sum].Add(i);  // add element to the array list
            }
        
            for (int j = SumArray.Length - 1; j > 0; j--)
            {
                if (SumArray[j] == true && j != FirstArray[i])
                {
                    if (Sum + j < SumArray.Length)
                    {
                        if (SumArray[Sum + j] == false)
                        {
                            foreach (var item in IndexArray[j]) //loop by j elements of array
                            {
                                IndexArray[Sum + j].Add(item); // adding j elements of list array to Sum+j elements

                            }
                            IndexArray[Sum + j].Add(i); // adding i elements to Sum+j elements
                        }
                        SumArray[Sum + j] = true;
                        
                    }
                }
            }         
         
            // if Searching sum is true break loop
            if (SumArray[S] == true)
            {
                break;
            }
        }
        // output to the console
        if (SumArray[S] == true)
        {
            Console.Write("Yes (");
            foreach (var item in IndexArray[S])
            {
                Console.Write(FirstArray[item]);
                if (item != IndexArray[S].Count)
                {
                    Console.Write(";");
                }

            }            
            Console.WriteLine(")");
        }
        else
        {
            Console.WriteLine("No");
        }*/


    }
    
}
