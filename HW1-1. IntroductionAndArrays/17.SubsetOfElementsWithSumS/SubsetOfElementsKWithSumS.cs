﻿using System;
using System.Collections.Generic;

class SubsetOfElementsKWithSumS
{
    static void Main()
    {
        Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }
        Console.WriteLine("Insert number S for desired sum");
        int S = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert number K for subset elements");
        int K = int.Parse(Console.ReadLine());
        bool[] SumArray = new bool[S + 1];
        List<int>[] IndexArray = new List<int>[S + 1];  // Define of Array with List elements for Subsums

        // Inicializing of the array of Lists
        for (int i = 0; i < S + 1; i++)
        {
            IndexArray[i] = new List<int>();
        }
        int Sum = 0;

        for (int i = 0; i < FirstArray.Length; i++)
        {
            Sum = FirstArray[i];
            if (Sum < SumArray.Length)
            {
                SumArray[Sum] = true;
                if (IndexArray[Sum].Count != 0)
                {
                    IndexArray[Sum].Clear();
                }
                IndexArray[Sum].Add(i);  // add element to the array list
            }            

            for (int j = SumArray.Length - 1; j > 0; j--)
            {
                if (SumArray[j] == true && j != FirstArray[i])
                {
                    if (Sum + j < SumArray.Length)
                    {
                        if (SumArray[Sum + j] == false)
                        {
                            foreach (var item in IndexArray[j]) //loop by j elements of array
                            {
                                IndexArray[Sum + j].Add(item); // adding j elements of list array to Sum+j elements

                            }
                            IndexArray[Sum + j].Add(i); // adding i elements to Sum+j elements
                        }
                        SumArray[Sum + j] = true;
                        
                    }
                }
            } 
            IndexArray[0][0] = 4;

            // if Searching sum is true break loop
            if (SumArray[S] == true)
            {
                // if Searching sum count is current amount break loop
                if (IndexArray[S].Count == K)
                {
                    break;
                }
                else
                {
                    IndexArray[S].Clear();
                    SumArray[S] = false;
                }
            }
        }
        // output to the console
        if (SumArray[S] == true)
        {
            Console.Write("Yes (");
            foreach (var item in IndexArray[S])
            {
                Console.Write(FirstArray[item]);
                if (item != IndexArray[S].Count)
                {
                    Console.Write(";");
                }

            }            
            Console.WriteLine(")");
        }
        else
        {
            Console.WriteLine("No");
        }
    }
}
