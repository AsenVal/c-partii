﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

class LongestIncreasingSequence
{
    public List<int> Calculate(List<int> deck)
    {
        //List<int> longestIncreasingSequence = new List<int>();
        List<Stack<LinkedNode<int>>> piles = new List<Stack<LinkedNode<int>>>();
        for (int i = 0; i < deck.Count; i++)
        {
            int addToPileNumber = DeterminePileToAddNumberTo(deck[i], piles);
            LinkedNode<int> data = new LinkedNode<int>(deck[i]);

            if (addToPileNumber == -1) //No suitable pile found. Create a new one
            {
                Stack<LinkedNode<int>> newStack = new Stack<LinkedNode<int>>();
                piles.Add(newStack);
                newStack.Push(data);
                if (piles.Count > 1)
                {
                    data.Next = piles[piles.Count - 2].Peek();
                }
            }
            else
            {
                if (addToPileNumber > 0)
                {
                    data.Next = piles[addToPileNumber - 1].Peek();
                }

                piles[addToPileNumber].Push(data);
            }
        }
       
        return GetSequenceFromLinkedNodes(piles[piles.Count - 1].Peek());
    }

    private List<int> GetSequenceFromLinkedNodes(LinkedNode<int> LinkedNode)
    {
        Stack<int> largestSequenceStack = new Stack<int>();
        largestSequenceStack.Push(LinkedNode.Data);
        while (LinkedNode.Next != null)
        {
            LinkedNode = LinkedNode.Next;
            largestSequenceStack.Push(LinkedNode.Data);
        }

        return largestSequenceStack.ToList();
    }

    private int DeterminePileToAddNumberTo(int number, List<Stack<LinkedNode<int>>> piles)
    {
        return piles.FindIndex(p => p.Peek().Data > number);
    }
}

/// <summary>
/// Stores the data and links to another node. Did not use LinkedListNode as it does not allow to set the next pointer.
/// </summary>
/// <typeparam name="T"></typeparam>
class LinkedNode<T>
{
    public LinkedNode()
    {
    }

    public LinkedNode(T data)
    {
        this.Data = data;
    }
    public T Data { get; set; }
    public LinkedNode<T> Next { get; set; }
}
class RemoveElementsToRemainIncreasingSortedArray
{        
    static void Main()
    {
        LongestIncreasingSequence lis = new  LongestIncreasingSequence();

        /*List<List<int>> matrix = new List<List<int>>();
        matrix.Add(new List<int>());
        matrix[0].Add(89);*/ 

        List<int> input = new List<int> { 5, 15, 1, 9, 3, 11, 14, 13, 18, 5 };
        List<int> result = lis.Calculate(input);

        Console.Write("{"+ result[0]);
        for (int i = 1; i < result.Count; i++)
        {
            Console.Write(",{0}",result[i]);
        }
        Console.WriteLine("}");

        //int[] FirstArray = new int[] { 6, 1, 4, 3, 0, 3, 6, 4, 5 };
        //findTheLongestIncreasingSequence(FirstArray);

        /*Console.WriteLine("Insert amount of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array elements:");
        int[] FirstArray = new int[n];
        for (int i = 0; i < n; i++)
        {
            FirstArray[i] = int.Parse(Console.ReadLine());
        }*/
        
        //for (int i = 0; i < InputArray.Count; i++)
        //{
        //    BufferArray.Add(i);
        //    FindLongestArray(i);
        //    BufferArray.Clear();
        //}
        //FindLongestArray(0);
        
        /*int[] FirstArray = new int[] { 6, 1, 4, 3, 0, 3, 6, 4, 5 };
        List<int>[] IndexArray = new List<int>[FirstArray.Length + 1];
        // Inicializing of the array of Lists
        for (int i = 0; i < FirstArray.Length + 1; i++)
        {
            IndexArray[i] = new List<int>();
        } 

        for (int i = 0; i < FirstArray.Length; i++)
        {
            IndexArray[i].Add(FirstArray[i]);
            for (int j = i + 1; j < FirstArray.Length - 1; j++)
            {
                if (FirstArray[j] >= IndexArray[i][IndexArray[i].Count - 1]) // izvikvane na poslednia element ot i-tia list
                {
                    IndexArray[i].Add(FirstArray[j]);
                }
            }
        }*/
        Console.WriteLine();
        
    }
    /*static void FindLongestArray(int k)
    {
        for (int i = k; i < InputArray.Count; i++)
        {
            BufferArray.Add(i);
            for (int j = k + 1; j < InputArray.Count - 1; j++)
            {
                if (InputArray[j] >= InputArray[BufferArray[BufferArray.Count - 1]])
                {
                    BufferArray.Add(j);
                }
            }
            if (BufferArray.Count > LongestArray.Count)
            {
                LongestArray.Clear();
                for (int j = 0; j < BufferArray.Count; j++)
                {
                    LongestArray.Add(BufferArray[j]);
                }
            }
            FindLongestArray(k + 1);
        }
    }*/
    
}
