﻿using System;

class PrintAllPermutationsOfN
{
    static void Main()
    {
        Console.WriteLine("Insert number N:");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("All permutations of N={0} ->",n);
        int[] onePermutation = new int[n];
        bool[] used = new bool[n];
        
        Permutation(onePermutation, used, 0);
    }

    static void Permutation(int[] onePermutation, bool[] used, int startNumber)
    {
        if (startNumber == onePermutation.Length)
        {
            print(onePermutation);
            return;
        }

        for (int j = 0; j < onePermutation.Length; j++)
        {
            if (used[j]) continue;

            onePermutation[startNumber] = j;

            used[j] = true;
            Permutation(onePermutation, used, startNumber + 1);
            used[j] = false;
        }
    }

    static void print(int[] onePermutation)
    {
        Console.Write("{");
        for (int i = 0; i < onePermutation.Length; i++)
        {
            if (i == onePermutation.Length - 1)
            {
                Console.WriteLine(onePermutation[i] + 1 + "}");
            }
            else
            {
                Console.Write(onePermutation[i] + 1 + ",");
            }
        }        
    }

}
