﻿using System;

class PrintAllKVariation
{
    static void Main()
    {
        Console.WriteLine("Insert number N:");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert number K for amount of variations:");
        int k = int.Parse(Console.ReadLine());
        Console.WriteLine("All Variations of N={0} ->", n);
        int[] oneVariation = new int[k];
     
        Variation(oneVariation, n, 0);
    }

    static void Variation(int[] oneVariation,int n, int startNumber)
    {
        if (startNumber == oneVariation.Length)
        {
            print(oneVariation);
            return;
        }

        for (int j = 0; j < n; j++)
        {
            oneVariation[startNumber] = j;

            Variation(oneVariation, n, startNumber + 1);
        }
    }

    static void print(int[] oneVariation)
    {
        Console.Write("{");
        for (int i = 0; i < oneVariation.Length; i++)
        {
            if (i == oneVariation.Length - 1)
            {
                Console.WriteLine(oneVariation[i] + 1 + "}");
            }
            else
            {
                Console.Write(oneVariation[i] + 1 + ",");
            }
        }
    }
}
