﻿using System;

class PrintAllKCombinations
{
    static void Main()
    {
        Console.WriteLine("Insert number N:");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert number K for amount of Combinations:");
        int k = int.Parse(Console.ReadLine());
        Console.WriteLine("All Combinations of N={0} ->", n);
        int[] oneCombination = new int[k];

        Combination(oneCombination, n, 0, 0);
    }

    static void Combination(int[] oneCombination, int n, int startNumber, int next)
    {
        if (startNumber == oneCombination.Length)
        {
            print(oneCombination);
            return;
        }

        for (int j = next; j < n; j++)
        {
            oneCombination[startNumber] = j;
            Combination(oneCombination, n, startNumber + 1, j + 1);
        }
    }

    static void print(int[] oneCombination)
    {
        Console.Write("{");
        for (int i = 0; i < oneCombination.Length; i++)
        {
            if (i == oneCombination.Length - 1)
            {
                Console.WriteLine(oneCombination[i] + 1 + "}");
            }
            else
            {
                Console.Write(oneCombination[i] + 1 + ",");
            }
        }
    }
}
