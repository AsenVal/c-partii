﻿using System;

class FillPrintMatrix
{
    static void Main()
    {
        Console.WriteLine("Insert row/columns of array");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert array type(a,b,c,d):");
        string arrtype = Console.ReadLine();
        int[,] DoubleArray = new int[n,n];
        int count = 1;
        if (arrtype == "a")
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    DoubleArray[j, i] = count++;
                }
            }
            PrintDoubleArrray(DoubleArray);
        }
        else if (arrtype == "b")
        {
            for (int i = 0; i < n; i++)
            {
                if (i % 2 == 1)
                {
                    for (int j = n-1; j >=0; j--)
                    {
                        DoubleArray[j, i] = count++;
                    }
                }
                else
                {
                    for (int j = 0; j < n; j++)
                    {
                        DoubleArray[j, i] = count++;
                    }
                }
            }
            PrintDoubleArrray(DoubleArray);
        }
        else if (arrtype == "c")
        {
           
            for (int j = 1; j <= n; j++)
            {
                for (int i = 0; i < j; i++)
                {
                    DoubleArray[n - j + i, i] = count++;
                }
            }
            for (int j = n-1; j >= 1; j--)
            {
                for (int i = n-j; i < n; i++)
                {
                    DoubleArray[j-n + i, i] = count++;
                }
            }
            PrintDoubleArrray(DoubleArray);
        }
        else if (arrtype == "d")
        {
            int position = 0;
            int broi = n;
            while (broi > n / 2)
            {

                for (int i = position; i < broi; i++)
                {
                    DoubleArray[i, position] = count;
                    count++;
                }
                for (int i = position; i < broi - 1; i++)
                {
                    DoubleArray[broi - 1, i + 1] = count;
                    count++;
                }
                for (int i = broi; i > position + 1; i--)
                {
                    DoubleArray[i - 2, broi - 1] = count;
                    count++;
                }
                for (int i = broi - 2; i > position; i--)
                {
                    DoubleArray[position, i] = count;
                    count++;
                }
                broi--;
                position++;
            }
            PrintDoubleArrray(DoubleArray);
        }
        else
        {
            Console.WriteLine("There is no such type");
        }
    }
    static void PrintDoubleArrray(int[,] DoubleArray)
    {
        for (int i = 0; i < DoubleArray.GetLength(0); i++)
        {
            for (int j = 0; j < DoubleArray.GetLength(0); j++)
            {
                Console.Write("{0,3}",DoubleArray[i,j]);
                Console.Write("{0,2}","|");
            }
            Console.WriteLine();
            for (int j = 0; j < DoubleArray.GetLength(0); j++)
            {
                Console.Write("-----");
            }
            Console.WriteLine();
        }
    }
}
