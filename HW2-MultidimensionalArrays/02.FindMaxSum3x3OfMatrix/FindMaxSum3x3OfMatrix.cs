﻿using System;

class FindMaxSum3x3OfMatrix
{
    static void Main()
    {
        Console.WriteLine("Insert number of rows of array N");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert number of columns of array M");
        int m = int.Parse(Console.ReadLine());
        int[,] DoubleArray = new int[n, m];
        Console.WriteLine("Insert first row elements");
        for (int i = 0; i < n; i++)
        {
            if (i != 0)
            {
                Console.WriteLine("Insert next row elements");
            }
            for (int j = 0; j < m; j++)
            {
                DoubleArray[i, j] = int.Parse(Console.ReadLine());
            }            
        }
        /*int[,] DoubleArray = {
                              {7, 1, 3, 3, 2, 1},
                              {1, 5, 3, 9, 8, 6},
                              {4, 1, 6, 7, 9, 0}, 
                             {2, 1, 4, 7, 6, 9} };*/
        int sum = 0;
        int largestSum = 0;
        int startrow = 0;
        int startcolumn = 0;
        for (int i = 0; i < DoubleArray.GetLength(0) - 2; i++)
        {
            for (int j = 0; j < DoubleArray.GetLength(1) - 2; j++)
            {
                for (int row = i; row < i + 3; row++)
                {
                    for (int col = j; col < j + 3; col++)
                    {
                        sum += DoubleArray[row, col];
                    }
                }
                if (sum > largestSum)
                {
                    largestSum = sum;
                    startrow = i;
                    startcolumn = j;
                }
                sum = 0;
            }
        }
        Console.WriteLine("Largest sum is {0}",largestSum);
        for (int i = startrow; i < startrow + 3; i++)
        {
            for (int j = startcolumn; j < startcolumn + 3; j++)
            {
                Console.Write("{0,3}", DoubleArray[i, j]);
                Console.Write("{0,2}", "|");
            }
            Console.WriteLine();
            for (int j = 0; j < 3; j++)
            {
                Console.Write("-----");
            }
            Console.WriteLine();
        }        

    }       
}
