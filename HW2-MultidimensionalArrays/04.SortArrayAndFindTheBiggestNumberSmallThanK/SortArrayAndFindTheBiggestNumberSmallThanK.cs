﻿using System;

class SortArrayAndFindTheBiggestNumberSmallThanK
{
    static void Main()
    {
        Console.WriteLine("Insert number of elements of array N");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert number K");
        int K = int.Parse(Console.ReadLine());
        int[] UnsortedArray = new int[n];
        Console.WriteLine("Insert elements of array");
        for (int i = 0; i < n; i++)
        {
            UnsortedArray[i] = int.Parse(Console.ReadLine());
        }
        //Array.Sort(UnsortedArray);
        Array.Sort(UnsortedArray, (x,y) => (x).CompareTo(y)); 

        int result = Array.BinarySearch(UnsortedArray, K);
        if (result < 0)
        {
            result = ~result - 1;
        }
        if (result >= 0)
        {
            Console.WriteLine("The biggest number smaler than {0} is {1}",K,UnsortedArray[result]);
        }
        else
        {
            Console.WriteLine("There is not an element smaller than K at this array");
        }
    }
}
