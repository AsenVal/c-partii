﻿using System;
using System.Collections.Generic;

class SortStringsByTheirLength
{
    static void Main()
    {
        Console.WriteLine("Insert number of elements of array N");
        int n = int.Parse(Console.ReadLine());
        string[] StringArray = new string[n];
        Console.WriteLine("Insert elements of string array");
        for (int i = 0; i < n; i++)
        {
            StringArray[i] = Console.ReadLine();
        }
        
        Array.Sort(StringArray, (x, y) => (x.Length).CompareTo(y.Length));
        Console.WriteLine("Sorted array is:");
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine(StringArray[i]);
        }



        //////

        List<int> input = new List<int> { 5,4,8,9,5,3,7,6,4,3,5}; // Define of List with List elements in it
        

        input.Sort(); // sort list ascending (List<int>) or lexicografski(List<string>)
        input.Reverse(); // Reverse list
        input.RemoveAt(2); // Remove second element of input list
        


    }
}
