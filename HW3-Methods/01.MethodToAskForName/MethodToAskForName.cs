﻿using System;
using System.Text;

namespace _01.MethodToAkForName
{
   public  class MethodToAskForName
    {
        public static void Main()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(AskForName());
            if (checkName(sb))
            {
                Console.WriteLine("Hello, {0}!", sb.ToString());
            }
            else
                Console.WriteLine("Incorect Name string");
        }

        public static string AskForName()
        {
            Console.WriteLine("Write your name:"); ;
            return (Console.ReadLine());
        }
        static bool checkName(StringBuilder sb)
        {
            bool result = true;

            for (int i = 0; i < sb.Length; i++)
            {
                result = (Char.IsLetter(sb[i]) || Char.IsWhiteSpace(sb[i]));
                if (result == false)
                    break;
            }
            return result;
        }
    }
}
