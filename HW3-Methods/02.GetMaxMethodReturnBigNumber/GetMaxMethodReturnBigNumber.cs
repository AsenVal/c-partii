﻿using System;

class GetMaxMethodReturnBigNumber
{
    static void Main()
    {
        int TheBiggest;
        Console.WriteLine("Insert first number to compare:");
        int SecondNumber = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert second number to compare:");
        int FirstNumber = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert third number to compare:");
        int ThirdNumber = int.Parse(Console.ReadLine());
        TheBiggest = GetMax(FirstNumber, SecondNumber);
        TheBiggest = GetMax(ThirdNumber, TheBiggest);
        
        Console.WriteLine("The biggest number of {0}; {1}; {2} is {3}", FirstNumber, SecondNumber, ThirdNumber, TheBiggest);
        

    }
    static int GetMax(int FirstNumber, int SecondNumber)
    {
        if (FirstNumber > SecondNumber)
        {
            return FirstNumber;
        }
        else
        {
            return SecondNumber;
        }
    }
}
