﻿using System;

class TranslateInEnglishLastDigit
{
    static void Main()
    {
        Console.WriteLine("Write number to translate its last digit:");
        string str = Console.ReadLine();
        Console.WriteLine(TranslateLastDigit(str));
    }

    static string TranslateLastDigit(string str)
    {
        switch (str.Substring(str.Length - 1))
        {
            case "0":
                return "zero";
            case "1":
                return "one";
            case "2":
                return "two";
            case "3":
                return "three";
            case "4":
                return "four";
            case "5":
                return "five";
            case "6":
                return "six";
            case "7":
                return "seven";
            case "8":
                return "eight";
            case "9":
                return "nine";
            default:
                return "Last symbol is not digit";
        }
    }
}
