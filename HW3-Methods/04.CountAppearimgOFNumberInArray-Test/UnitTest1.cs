﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace _04.CountAppearimgOFNumberInArray_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int[] arr = new int[]{2,3,4,5,6,8,8,4,6,2,4,5,5,6,7,4,5,6,7};
            int number = CountAppearimgOFNumberInArray.countAppearimgOFNumber(arr, 5);
            Assert.AreEqual(number, 4);
        }
    }
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestMethod2()
        {
            int[] arr = new int[] { 2, 3, 4, 5, 6, 8, 8, 4, 6, 2, 6, 5, 5, 6, 7, 4, 5, 6, 7 };
            int number = CountAppearimgOFNumberInArray.countAppearimgOFNumber(arr, 6);
            Assert.AreEqual(number, 5);
        }
    }
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void TestMethod3()
        {
            int[] arr = new int[] { 2, 3, 4, 5, 6, 8, 8, 4, 6, 2, 6, 5, 5, 6, 7, 4, 5, 6, 7 };
            int number = CountAppearimgOFNumberInArray.countAppearimgOFNumber(arr, 7);
            Assert.AreEqual(number, 2);
        }
    }
    [TestClass]
    public class UnitTest4
    {
        [TestMethod]
        public void TestMethod4()
        {
            int[] arr = new int[] { 2, 3, 4, 5, 6, 8, 8, 4, 6, 2, 6, 5, 5, 6, 7, 4, 5, 6, 7 };
            int number = CountAppearimgOFNumberInArray.countAppearimgOFNumber(arr, 10);
            Assert.AreEqual(number, 0);
        }
    }
}
