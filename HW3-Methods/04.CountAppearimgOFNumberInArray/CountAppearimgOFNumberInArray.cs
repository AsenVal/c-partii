﻿using System;

public class CountAppearimgOFNumberInArray
{
    public static void Main()
    {
        Console.WriteLine("Insert amount of array N:");
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert elements of array:");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++)
        {
            arr[i] = int.Parse(Console.ReadLine());
        }
        Console.WriteLine("Insert searching numer:");
        int snumber = int.Parse(Console.ReadLine());

        int count = countAppearimgOFNumber(arr, snumber);
        Console.WriteLine(count);
    }

    public static int countAppearimgOFNumber(int[] arr, int s)
    {
        int counter = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == s)
            {
                counter++;
            }
        }
        return counter;
    }
}
