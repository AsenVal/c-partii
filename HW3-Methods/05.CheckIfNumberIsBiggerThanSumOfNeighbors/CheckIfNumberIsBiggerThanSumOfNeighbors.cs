﻿using System;

class CheckIfNumberIsBiggerThanSumOfNeighbors
{
    static void Main()
    {
        int n;
        do
        {
            Console.WriteLine("Insert amount of array N > 2:");
            n = int.Parse(Console.ReadLine());
        }
        while (n < 3);
        Console.WriteLine("Insert elements of array:");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++)
        {
            arr[i] = int.Parse(Console.ReadLine());
        }
        Console.WriteLine("Insert position of element:");
        int position = int.Parse(Console.ReadLine());

        bool IsBigger = IsNumberBiggerThanNeighbors(arr, position);
        if (IsBigger == true)
        {
            Console.WriteLine("Number {0} is bigger than its neighbours.", arr[position]);
        }
        else
        {
            Console.WriteLine("Number {0} is smaller than its neighbours.", arr[position]);
        }
    }

    static bool IsNumberBiggerThanNeighbors(int[] arr, int position)
    {
        bool counter = false;
        int previous;
        int next;
        if (position == 0)
        {
            previous = 0;
            next = arr[position + 1];
        }
        else if (position == arr.Length - 1)
        {
            previous = arr[position - 1];
            next = 0;
        }
        else
        {
            previous = arr[position - 1];
            next = arr[position + 1];
        }

        if (arr[position] > previous + next)
        {
            counter = true;
        }

        return counter;
    }
    
}
