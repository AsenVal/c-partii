﻿using System;

class ReturnIndexOfElementThatIsBiggerThanNeighbours
{
    static void Main()
    {
        // input
        int n;
        int index;
        do
        {
            Console.WriteLine("Insert amount of array N > 2:");
            n = int.Parse(Console.ReadLine());
        }
        while (n < 3);
        int[] arr = new int[n];
        Console.WriteLine("Insert elements of array:");
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = int.Parse(Console.ReadLine());
        }

        // solution
        index = IndexOfFirstBiggerElements(arr);        
        Console.WriteLine("The index of first Number bigger than its neighbours is {0}.", index);
    }

    private static int IndexOfFirstBiggerElements(int[] arr)
    {        
        for (int i = 0; i < arr.Length; i++)
        {
            if (IsNumberBiggerThanNeighbors(arr, i))
            {
                return i;
            }
        }
        return -1;
    }

    static bool IsNumberBiggerThanNeighbors(int[] arr, int position)
    {
        bool counter = false;
        int previous;
        int next;
        if (position == 0)
        {
            previous = 0;
            next = arr[position + 1];
        }
        else if (position == arr.Length - 1)
        {
            previous = arr[position - 1];
            next = 0;
        }
        else
        {
            previous = arr[position - 1];
            next = arr[position + 1];
        }

        if (arr[position] > previous + next)
        {
            counter = true;
        }

        return counter;
    }
}
