﻿using System;

class ReverseDigitsOfNumber
{
    static void Main()
    {
        Console.WriteLine("Insert number to reverse:");
        double n = double.Parse(Console.ReadLine());
        n = ReveerseDecimalNumber(n);
        Console.WriteLine(n);
    }

    static double ReveerseDecimalNumber(double n)
    {
        string str = n.ToString();
        string result = "";
        for (int i = str.Length - 1; i >= 0; i--)
        {
            result += (str.Substring(i, 1));
        }
        return Convert.ToDouble(result);
    }
}
