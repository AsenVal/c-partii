﻿using System;
using System.Text;

class SumOfTwoNumbersWhichDigitsAreInArray
{
    static void Main()
    {

        //StringBuilder firstNumber = new StringBuilder();
        //StringBuilder secondNumber = new StringBuilder();
        Console.WriteLine("Insert first number:");
        string firstNumber = Console.ReadLine();
        Console.WriteLine("Insert second number:");
        string secondNumber =  Console.ReadLine();
        string result = SumTwoNumbersByDigits(firstNumber, secondNumber);
        Console.WriteLine(result);
    }

    static string SumTwoNumbersByDigits(string firstNumber, string secondNumber)
    {
        int[] firstArray = new int[Math.Max(firstNumber.Length, secondNumber.Length)];
        int[] secondArray = new int[Math.Max(firstNumber.Length, secondNumber.Length)];
        int[] sumArray = new int[Math.Max(firstNumber.Length, secondNumber.Length) + 1];
        StringBuilder Number = new StringBuilder();
        Number.Append(firstNumber);
        for (int i = 0; i < firstNumber.Length; i++)
        {
            firstArray[i] = Convert.ToInt32(Number[firstNumber.Length - 1 - i].ToString());
        }
        Number.Clear();
        Number.Append(secondNumber);
        for (int i = 0; i < secondNumber.Length; i++)
        {
            secondArray[i] = Convert.ToInt32(Number[secondNumber.Length - 1 - i].ToString());
        }
        Number.Clear();
        int naum = 0;
        for (int i = 0; i < Math.Max(firstNumber.Length, secondNumber.Length) + 1; i++)
        {
            if (i == 0)
            {
                sumArray[i] = (firstArray[i] + secondArray[i]) % 10;
            }
            else if (i == Math.Max(firstNumber.Length, secondNumber.Length))
            {
                sumArray[i] = (firstArray[i - 1] + secondArray[i - 1]) / 10;
            }
            else
            {
                sumArray[i] = (firstArray[i] + secondArray[i]) % 10 + (firstArray[i - 1] + secondArray[i - 1]) / 10 + naum;
                naum = 0;
                if (sumArray[i] > 9)
                {
                    naum = sumArray[i] / 10;
                    sumArray[i] = sumArray[i] % 10;
                }
            }
        }
        Array.Reverse(sumArray);
        if (sumArray[0] != 0)
        {
            Number.Append(sumArray[0]);
        }
        for (int i = 1; i < Math.Max(firstNumber.Length, secondNumber.Length) + 1; i++)
        {
            Number.Append(sumArray[i]);
        }
        return Number.ToString();
    }
}
