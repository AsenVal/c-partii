﻿using System;

class MaxElementInPortionalArray
{
    static void Main()
    {
        int[] arr = new int[] {2,5,6,9,12,5,3,8,7,6,9,10,5,8,18,25,28,6,22,78,32,65,45};
        int index;

        PrintArray(arr);
        index = GetMaxOFArray(arr, 10);
        Console.WriteLine(arr[index]);
        SortArrayDescending(arr);
        PrintArray(arr);
        SortArrayAscending(arr);
        PrintArray(arr);
    }

    private static void PrintArray(int[] arr)
    {
        Console.Write(arr[0]);
        for (int i = 1; i < arr.Length; i++)
        {
            Console.Write(",{0}", arr[i]);
        }
        Console.WriteLine();
    }

    private static void SortArrayDescending(int[] arr)
    {
        int index;
        int buffer;
        for (int i = 0; i < arr.Length; i++)
        {
            index = GetMaxOFArray(arr, i);
            if (index != i)
            {
                buffer = arr[i];
                arr[i] = arr[index];
                arr[index] = buffer;
            }
        }
    }

    private static void SortArrayAscending(int[] arr)
    {
        SortArrayDescending(arr);
        Array.Reverse(arr);
    }


    private static int GetMaxOFArray(int[] arr, int p)
    {
        int index = p;
        for (int i = p+1; i < arr.Length; i++)
        {
            if (ISSecondBigger(arr[index], arr[i]))
            {
                index = i;
            }
        }
        return index;
    }
    static bool ISSecondBigger(int FirstNumber, int SecondNumber)
    {
        if (FirstNumber < SecondNumber)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
