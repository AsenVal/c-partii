﻿using System;
using System.Collections.Generic;

class ProgramSolveTasks
{
    static void Main()
    {
        Console.WriteLine("Choose an option:");
        Console.WriteLine("   - Press R to reverse digits of number:");
        Console.WriteLine("   - Press C to calculate average of sequence of integers:");
        Console.WriteLine("   - Press S to solve linear equation a * x + b = 0:");
        string choise = Console.ReadLine();
        choise = choise.ToUpper();

        while (choise != "R" && choise != "C" && choise != "S")
        {
            Console.WriteLine("Press \"R\" or \"C\" or \"S\"");
            choise = Console.ReadLine();
            choise = choise.ToUpper();
        }
        switch (choise)
        {
            case "R":
                Console.WriteLine("Insert number to reverse:");
                double n = double.Parse(Console.ReadLine());
                n = ReveerseDecimalNumber(n);
                Console.WriteLine(n);
                break;
            case "C":
                Console.WriteLine("Insert sequence of integers separated by \",\":");
                string sequence = Console.ReadLine();
                List<int> numbers = new List<int>();
                int count = 0;
                while (sequence != "")
                {
                    if (sequence.Substring(count, 1) == ",")
                    {
                        numbers.Add(Convert.ToInt32(sequence.Substring(0, count)));
                        sequence = sequence.Substring(count + 1);
                        count = 0;
                    }
                    count++;
                    if (count >= sequence.Length && sequence != "")
                    {
                        numbers.Add(Convert.ToInt32(sequence));
                        sequence = "";
                    }
                }
                double result = FindAvarageNumber(numbers);
                Console.WriteLine(result);
                break;
            case "S":
                Console.WriteLine("Insert parametars for linear equation a * x + b = 0:");
                Console.WriteLine("Insert a:");
                double a;
                double b;
                while (double.TryParse(Console.ReadLine(), out a) == false)
                {
                    Console.WriteLine("You must insert a number. Insert a:");
                };
                Console.WriteLine("Insert b:");
                while (double.TryParse(Console.ReadLine(), out b) == false)
                {
                    Console.WriteLine("You must insert a number. Insert b:");
                };
                double resultS = CalculateLinearEquation(a, b);
                Console.WriteLine("x = {0}",resultS);
                break;
        }

    }

    private static double CalculateLinearEquation(double a, double b)
    {
        double x;
        x = (-1) * b / a;
        return x;
    }

    private static double FindAvarageNumber(List<int> numbers)
    {
        int result = 0;
        for (int i = 0; i < numbers.Count; i++)
        {
            result += numbers[i];
        }
        return (double) result / numbers.Count;       
    }
    static double ReveerseDecimalNumber(double n)
    {
        string str = n.ToString();
        string result = "";
        for (int i = str.Length - 1; i >= 0; i--)
        {
            result += (str.Substring(i, 1));
        }
        return Convert.ToDouble(result);
    }

    public static double resultS { get; set; }
}
