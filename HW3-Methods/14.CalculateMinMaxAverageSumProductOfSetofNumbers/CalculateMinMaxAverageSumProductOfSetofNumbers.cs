﻿using System;
using System.Collections.Generic;

class CalculateMinMaxAverageSumProductOfSetofNumbers
{
    static void Main()
    {
        Console.WriteLine("Insert sequence of integers separated by \",\":");
        string sequence = Console.ReadLine();
        List<int> numbers = new List<int>();
        int count = 0;
        while (sequence != "")
        {
            if (sequence.Substring(count, 1) == ",")
            {
                numbers.Add(Convert.ToInt32(sequence.Substring(0, count)));
                sequence = sequence.Substring(count + 1);
                count = 0;
            }
            count++;
            if (count >= sequence.Length && sequence != "")
            {
                numbers.Add(Convert.ToInt32(sequence));
                sequence = "";
            }
        }
        int minresult = FindMinNumber(numbers);
        Console.WriteLine("Min element is: {0}", minresult);
        int maxresult = FindMaxNumber(numbers);
        Console.WriteLine("Max element is: {0}", maxresult);
        double avarageresult = FindAvarageNumber(numbers);
        Console.WriteLine("Average of elements is: {0}", avarageresult);
        int sumresult = FindSumNumber(numbers);
        Console.WriteLine("Sum of elements is: {0}", sumresult);
        int productresult = FindProductNumber(numbers);
        Console.WriteLine("Product of elements is: {0}", productresult);
    }

    private static int FindMinNumber(List<int> numbers)
    {
        return numbers[GetMinOFArray(numbers)];
    }
    
    private static int FindMaxNumber(List<int> numbers)
    {
       return numbers[GetMaxOFArray(numbers)];
    }

    private static double FindAvarageNumber(List<int> numbers)
    {
        int result = 0;
        for (int i = 0; i < numbers.Count; i++)
        {
            result += numbers[i];
        }
        return (double)result / numbers.Count;
    }

    private static int FindSumNumber(List<int> numbers)
    {
        int result = 0;
        for (int i = 0; i < numbers.Count; i++)
        {
            result += numbers[i];
        }
        return result;
    }

    private static int FindProductNumber(List<int> numbers)
    {
        int result = 1;
        for (int i = 0; i < numbers.Count; i++)
        {
            result *= numbers[i];
        }
        return result;
    }

    private static int GetMaxOFArray(List<int> arr)
    {
        int index = 0;
        for (int i = 1; i < arr.Count; i++)
        {
            if (arr[index] < arr[i])
            {
                index = i;
            }
        }
        return index;
    }
    private static int GetMinOFArray(List<int> arr)
    {
        int index = 0;
        for (int i = 1; i < arr.Count; i++)
        {
            if (arr[index] > arr[i])
            {
                index = i;
            }
        }
        return index;
    }
}
