﻿using System;
using System.Collections.Generic;

class CalculateMinMaxAverageSumProductForAllTypes
{
    static void Main()
    {
        // define diferent type of object (byre, int, long, double .... ) and print
        Console.WriteLine("Integer values");
        PrintElements(new List<object>() { 2, 5, 9, 7 });
        Console.WriteLine();
        Console.WriteLine("float values");
        PrintElements(new List<object>() { 2.52f, 5.265f, 9.255f, 8f });
        Console.WriteLine();
        Console.WriteLine("decimal values");
        PrintElements(new List<object>() { 2.52M, 5.265514161151651515M, 9.2515538511568684855135M, 8M });
    }

    private static void PrintElements(List<object> numbers)
    {
        string minresult = FindMinNumber(numbers).ToString();
        Console.WriteLine("Min element is: {0}", minresult);
        string maxresult = FindMaxNumber(numbers).ToString();
        Console.WriteLine("Max element is: {0}", maxresult);
        string avarageresult = FindAvarageNumber(numbers).ToString();
        Console.WriteLine("Average of elements is: {0}", avarageresult);
        string sumresult = FindSumNumber(numbers).ToString();
        Console.WriteLine("Sum of elements is: {0}", sumresult);
        string productresult = FindProductNumber(numbers).ToString();
        Console.WriteLine("Product of elements is: {0}", productresult);
    }

    private static T FindMinNumber<T>(List<T> numbers)
    {
        return numbers[GetMinOFArray(numbers)];
    }

    private static T FindMaxNumber <T>(List<T> numbers)
    {
        return numbers[GetMaxOFArray(numbers)];
    }

    private static double FindAvarageNumber<T>(List<T> numbers)
    {
        dynamic result = 0;
        for (int i = 0; i < numbers.Count; i++)
        {
            result += (dynamic) numbers[i];
        }
        return (double)result / numbers.Count;
    }

    private static T FindSumNumber<T>(List<T> numbers)
    {
        dynamic result = 0;
        for (int i = 0; i < numbers.Count; i++)
        {
            result += (dynamic) numbers[i];
        }
        return result;
    }

    private static T FindProductNumber<T>(List<T> numbers)
    {
        dynamic result = 1;
        for (int i = 0; i < numbers.Count; i++)
        {
            result *= (dynamic) numbers[i];
        }
        return result;
    }

    private static int GetMaxOFArray<T>(List<T> arr)
    {
        int index = 0;
        for (int i = 1; i < arr.Count; i++)
        {
            if ((dynamic) arr[index] < (dynamic) arr[i])
            {
                index = i;
            }
        }
        return index;
    }
    private static int GetMinOFArray<T>(List<T> arr)
    {
        int index = 0;
        for (int i = 1; i < arr.Count; i++)
        {
            if ((dynamic)arr[index] > (dynamic)arr[i])
            {
                index = i;
            }
        }
        return index;
    }
}
