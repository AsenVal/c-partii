﻿using System;
using System.Collections.Generic;
using System.Text;


class ConvertDecimalToBinary
{
    static void Main()
    {
        ConvertNumeralSystem NS = new ConvertNumeralSystem();
        Console.WriteLine("Enter Number in Decimal to convert in Binary Numeric System:");
        string n = Console.ReadLine();
        string result;

        NS.DecimalToBinary(n, 10, 2, out result);
        Console.WriteLine(result);
    }
    
}
