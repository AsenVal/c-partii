﻿using System;

class ConvertBinaryToDecimal
{
    static void Main(string[] args)
    {
        ConvertNumeralSystem NS = new ConvertNumeralSystem();
        Console.WriteLine("Enter Number in Binary to convert in Decimal Numeric System:");
        string n = Console.ReadLine();
        string result;

        NS.DecimalToBinary(n, 2, 10, out result);
        Console.WriteLine(result);
    }
}
