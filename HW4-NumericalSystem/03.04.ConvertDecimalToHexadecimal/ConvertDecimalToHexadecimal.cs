﻿using System;

class ConvertDecimalToHexadecimal
{
    static void Main()
    {
        ConvertNumeralSystem NS = new ConvertNumeralSystem();
        Console.WriteLine("Enter Number in Decimal to convert in Hexadecimal Numeric System:");
        string n = Console.ReadLine();
        string result;

        NS.DecimalToBinary(n, 10, 16, out result);
        Console.WriteLine(result);
    }
}
