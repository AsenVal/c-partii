﻿using System;

class ConvertHexadecimalToDecimal
{
    static void Main()
    {
        ConvertNumeralSystem NS = new ConvertNumeralSystem();
        Console.WriteLine("Enter Number in Hexadecimal to convert in Decimal Numeric System:");
        string n = Console.ReadLine();
        string result;

        NS.DecimalToBinary(n, 16, 10, out result);
        Console.WriteLine(result);
    }
}
