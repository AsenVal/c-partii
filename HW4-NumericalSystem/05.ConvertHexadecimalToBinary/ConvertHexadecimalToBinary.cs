﻿using System;
using System.Collections.Generic;
using System.Text;

class ConvertHexadecimalToBinary
{
    static void Main()
    {
        Console.WriteLine("Enter Number in Hexadecimal to convert in Binary Numeric System:");
        string str = Console.ReadLine();
        string simbol;
        List<string> arr = new List<string>();
        for (int i = 0; i < str.Length; i++)
        {
            simbol = str.Substring(i, 1);
            switch (simbol)
            {
                case "0":
                    arr.Add("0000");
                    break;
                case "1":
                    arr.Add("0001");
                    break;
                case "2":
                    arr.Add("0010");
                    break;
                case "3":
                    arr.Add("0011");
                    break;
                case "4":
                    arr.Add("0100");
                    break;
                case "5":
                    arr.Add("0101");
                    break;
                case "6":
                    arr.Add("0110");
                    break;
                case "7":
                    arr.Add("0111");
                    break;
                case "8":
                    arr.Add("1000");
                    break;
                case "9":
                    arr.Add("1001");
                    break;
                case "A":
                    arr.Add("1010");
                    break;
                case "B":
                    arr.Add("1011");
                    break;
                case "C":
                    arr.Add("1100");
                    break;
                case "D":
                    arr.Add("1101");
                    break;
                case "E":
                    arr.Add("1110");
                    break;
                case "F":
                    arr.Add("1111");
                    break;
            }
        }        
        arr.Reverse();

        StringBuilder builder = new StringBuilder();
        foreach (string n in arr)
        {
            builder.Append(n);
        }
        while (builder[0] == '0')
        {
            builder.Remove(0, 1);
        }
        Console.WriteLine(builder.ToString());
    }
}
