﻿using System;
using System.Text;

class BinaryRepresentationOfShortNumber
{
    static void Main()
    {
        ConvertNumeralSystem NS = new ConvertNumeralSystem();
        Console.WriteLine("Enter 16-bit signed integer to shows its binary representation:");
        string n = Console.ReadLine();
        string result;

        
        NS.DecimalToBinary(n, 10, 2, out result);
        if (n.Substring(0, 1) != "-")
        {
            StringBuilder builder = new StringBuilder();
            int bits = NS.CheckBitsOfNumber(Convert.ToInt32(n), 2);
            builder.Append(result);
            builder.Insert(0, "0", bits - builder.Length);
            result = builder.ToString();
        }
        Console.WriteLine(result);
    }
}
