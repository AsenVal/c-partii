﻿using System;
using System.Globalization;
using System.Text;

class BinaryRepresentationOfFloatNumber
{
    static void Main()
    {
        ConvertNumeralSystem NS = new ConvertNumeralSystem();
        Console.WriteLine("Enter 32-bit signed floating-point number to shows its binary representation:");
        string n = Console.ReadLine();
        string sign = "0";
        string exponent;
        string mantissa;
        StringBuilder builder = new StringBuilder();
        if (n.Substring(0, 1) == "-")
        {
            sign = "1";
            n = n.Substring(1);
        }
        int index = n.IndexOf(CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);
        if (index == -1)
        {
            exponent = n;
            mantissa = "0.0";
        }
        else
        {
            exponent = n.Substring(0, index);
            mantissa = "0" + n.Substring(index);
        }
        NS.DecimalToBinary(exponent, 10, 2, out exponent);
        mantissa = NS.ConvertFractionFromDecimalToBinary(Convert.ToDouble(mantissa), 2);
        if (exponent != "0")
        {
            index = exponent.Length - (exponent.IndexOf("1") + 1);
            builder.Append(exponent.Substring(index));
            builder.Append(mantissa);
        }
        else
        {
            index = mantissa.IndexOf("1") + 1;
            builder.Append(mantissa.Substring(index));
            index = (-1) * index;
        }
        

        if (builder.Length < 23)
        {            
            builder.Insert(builder.Length, "0", 23 - builder.Length);
        } 
        else
        {
            if (builder[23] == '1')
            {
                builder[22] = '1';
            }
            builder.Remove(builder.Length - 1, 1);

        }
        mantissa = builder.ToString();
        NS.DecimalToBinary(Convert.ToString(127+index), 10, 2, out exponent);
        if (exponent.Length < 9)
        {
            builder.Clear();
            builder.Append(exponent);
            builder.Insert(0, "0", 8 - builder.Length);
            exponent = builder.ToString();
        }

        Console.WriteLine("Sign: {0}", sign);
        Console.WriteLine("Exponent: {0}", exponent);
        Console.WriteLine("Mantissa: {0}", mantissa);
    }
}
