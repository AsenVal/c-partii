﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Test
    {
        static void Main(string[] args)
        {
            string str = "abcdefghijklmnopqrstuvwxyz";
            for (int i = 0; i < 10; i++)
			{
                for (int j = 0; j < str.Length; j++)
                {                    
			    Console.Write("\"{0}{1}\",",str.Substring(i,1),str.Substring(j,1).ToUpper());
                }
			}
            Console.WriteLine();
            //Console.WriteLine(ConvertDigit(11));
            //Console.WriteLine(ConvertDigit("B"));
        }
        static string ConvertDigit(int number)
        {
            string result = "";
            List<string> Letters = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            result = Letters[number - 1];
            return result;
            //abcdefghijklmnopqrstuvwxyz
        }
        static int ConvertDigit(string number)
        {
            int result = 0;
            List<string> Letters = new List<string>() {
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                "aA","aB","aC","aD","aE","aF","aG","aH","aI","aJ","aK","aL","aM","aN","aO","aP",
                "aQ","aR","aS","aT","aU","aV","aW","aX","aY","aZ","bA","bB","bC","bD","bE","bF",
                "bG","bH","bI","bJ","bK","bL","bM","bN","bO","bP","bQ","bR","bS","bT","bU","bV",
                "bW","bX","bY","bZ","cA","cB","cC","cD","cE","cF","cG","cH","cI","cJ","cK","cL",
                "cM","cN","cO","cP","cQ","cR","cS","cT","cU","cV","cW","cX","cY","cZ","dA","dB",
                "dC","dD","dE","dF","dG","dH","dI","dJ","dK","dL","dM","dN","dO","dP","dQ","dR",
                "dS","dT","dU","dV","dW","dX","dY","dZ","eA","eB","eC","eD","eE","eF","eG","eH",
                "eI","eJ","eK","eL","eM","eN","eO","eP","eQ","eR","eS","eT","eU","eV","eW","eX",
                "eY","eZ","fA","fB","fC","fD","fE","fF","fG","fH","fI","fJ","fK","fL"};
            result = Letters.IndexOf(number) + 1;
            return result;
            //abcdefghijklmnopqrstuvwxyz
        }
    }
}
