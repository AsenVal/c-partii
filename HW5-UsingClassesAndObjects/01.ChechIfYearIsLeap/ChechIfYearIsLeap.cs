﻿using System;

class ChechIfYearIsLeap
{
    static void Main()
    {
        Console.WriteLine("Enter year: ");
        int year = int.Parse(Console.ReadLine());

        if (DateTime.IsLeapYear(year))
        {
            Console.WriteLine("The year {0} is Leap!", year);
        }
        else
        {
            Console.WriteLine("The year {0} is not Leap!", year);
        }
    }
}
