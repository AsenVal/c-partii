﻿using System;

class PrintWhichDayOfWeekIsToday
{
    static void Main()
    {
        DateTime today = DateTime.Today;
        Console.WriteLine("Today is {0}.",today.DayOfWeek.ToString());
    }
}
