﻿using System;

class CalculateSurfaceOfTriangle
{
    static void Main()
    {
        Console.WriteLine("Calculating the surface of an triangle: ");
        Console.WriteLine("1. by side and altitude ");
        Console.WriteLine("2. by 3 sides");
        Console.WriteLine("3. by 2 sides and an angle between them in degrees)");
        Console.WriteLine("Choose from 1 to 3.");
        int choice = int.Parse(Console.ReadLine());
        double result;

        if (choice == 1)
        {
            Console.Write("Enter side: ");
            double side = double.Parse(Console.ReadLine());
            Console.Write("Enter altitude: ");
            double atitude = double.Parse(Console.ReadLine());
            result = TriangleSurfaceSideAndAtitude(side, atitude);
            Console.WriteLine("The surface is: {0}", result);
        }
        else if (choice == 2)
        {
            Console.Write("Enter side: ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("Enter side: ");
            double b = double.Parse(Console.ReadLine());
            Console.Write("Enter side: ");
            double c = double.Parse(Console.ReadLine());
            result = TriangleSurfaceThreeSides(a, b, c);
            Console.WriteLine("The surface is: {0}", result);
        }
        else if (choice == 3)
        {
            Console.Write("Enter side: ");
            double side = double.Parse(Console.ReadLine());
            Console.Write("Enter side: ");
            double side2 = double.Parse(Console.ReadLine());
            Console.Write("Enter angle: ");
            double angle = double.Parse(Console.ReadLine());
            result = TriangleSurfaceTwoSidesAndAngle(side, side2, angle);
            Console.WriteLine("The surface is: {0}", result);
        }
        else
        {
            Console.WriteLine("Invalid Choice !!!");
        }
    }
    static double TriangleSurfaceSideAndAtitude(double a, double h)
    {
        return ((a * h) / 2);
    }
    static double TriangleSurfaceThreeSides(double a, double b, double c)
    {
        double p = ((a + b + c) / 2);
        return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
    }
    static double TriangleSurfaceTwoSidesAndAngle(double a, double b, double alfa)
    {
        double pi = Math.PI;
        double sin = Math.Sin((alfa * pi) / 180);
        return ((a * b * sin) / 2);
    }
}
