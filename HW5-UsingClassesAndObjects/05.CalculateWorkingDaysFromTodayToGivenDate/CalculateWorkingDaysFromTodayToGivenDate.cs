﻿using System;
using System.Globalization;

class CalculateWorkingDaysFromTodayToGivenDate
{
    static DateTime[] holidays =
        {
            new DateTime(1/5/2013),
            new DateTime(2/5/2013),
            new DateTime(3/5/2013),
            new DateTime(6/5/2013),
            new DateTime(24/5/2013),
            new DateTime(6/9/2013),
            new DateTime(24/12/2013),
            new DateTime(25/12/2013),
            new DateTime(26/12/2013),
            new DateTime(31/12/2013)
        };
    static void Main()
    {
        
        Console.WriteLine("Enter a end date in DD/MM/YYYY format");
        DateTime endDate = DateTime.ParseExact(Console.ReadLine(), new string[] { "d/M/yyyy", "dd/MM/yyyy", "d/m/yyyy", "dd/mm/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        DateTime startDate = DateTime.Today;
        int result = WorkingDays(startDate, endDate);

        Console.WriteLine(result);
    }

    static int WorkingDays(DateTime startDate, DateTime endDate)
    {
        if (startDate.DayOfWeek == DayOfWeek.Saturday)
        {
            startDate = startDate.AddDays(2);
        }
        if (startDate.DayOfWeek == DayOfWeek.Sunday)
        {
            startDate = startDate.AddDays(1);
        }
        if (endDate.DayOfWeek == DayOfWeek.Saturday)
        {
            endDate = endDate.AddDays(-1);
        }
        if (endDate.DayOfWeek == DayOfWeek.Sunday)
        {
            endDate = endDate.AddDays(-2);
        }
        int days = (int)(endDate - startDate).TotalDays + 1;
        days = days / 7 * 5 + days % 7;
        days = FilterHolidays(startDate, endDate, days);
        return days;
    }

    static int FilterHolidays(DateTime start, DateTime end, int result)
    {
        foreach (DateTime holiday in holidays)
        {
            if (start <= holiday && holiday <= end && !(holiday.DayOfWeek == DayOfWeek.Saturday || holiday.DayOfWeek == DayOfWeek.Sunday))
            {
                result--;
            }
        }
        return result;
    }
}
