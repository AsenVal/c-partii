﻿using System;

class CalcSumOfStringOfNumbers
{
    static void Main()
    {
        string str = "43  68 9 23 318";
        int sum = 0;
        string[] strNums = str.Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < strNums.Length; i++)
        {
            sum = sum + int.Parse(strNums[i]);
        }
        Console.WriteLine(sum);
    }
}
