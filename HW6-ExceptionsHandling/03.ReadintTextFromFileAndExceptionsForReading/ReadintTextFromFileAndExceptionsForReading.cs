﻿using System;
using System.IO;
class ReadintTextFromFileAndExceptionsForReading
{
    static void Main()
    {
        Console.WriteLine("Enter full path to file");
        string str = Console.ReadLine();
        try
        {
            string result = File.ReadAllText(str, System.Text.Encoding.Unicode);
            Console.WriteLine(result);
        }
        catch (PathTooLongException)
        {
            Console.Error.WriteLine("Error: The specified path, file name, or both exceed the system-defined maximum length");
        }
        catch (DirectoryNotFoundException)
        {
            Console.Error.WriteLine("Error: The specified path is invalid (for example, it is on an unmapped drive)");
        }
        catch (UnauthorizedAccessException)
        {
            Console.Error.WriteLine(@"Error: Path specified a file that is read-only.
-or-
This operation is not supported on the current platform.
-or-
path specified a directory.
-or-
The caller does not have the required permission. ");
        }
        catch (FileNotFoundException)
        {
            Console.Error.WriteLine("Error: The file specified in path was not found. ");
        }
        catch (ArgumentNullException)
        {
            Console.Error.WriteLine("Error: Path is null");
        }
        catch (ArgumentException)
        {
            Console.Error.WriteLine("Error: Path is a zero-length string, contains only white space, or contains one or more invalid characters");
        }
        catch (IOException)
        {
            Console.Error.WriteLine("Error: An I/O error occurred while opening the file. ");
        }
        catch (NotSupportedException)
        {
            Console.Error.WriteLine("Error: Path is in an invalid format. ");
        }
        catch (System.Security.SecurityException)
        {
            Console.Error.WriteLine("Error: The caller does not have the required permission. ");
        }

    }
}
