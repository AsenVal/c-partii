﻿using System;
using System.Net;
using System.IO;

class DownloadFileFromInternetAndExceptionsForThat
{
    static void Main()
    {
        string file = "http://www.devbg.org/img/Logo-BASD.jpg";
        WebClient webClient = new WebClient();
        try
        {
            webClient.DownloadFile(file, Directory.GetCurrentDirectory() + "/Logo-BASD.jpg" );
            Console.WriteLine("File is downloaded!");
        }
        catch (WebException)
        {
            Console.Error.WriteLine(@"Error: The URI formed by combining BaseAddress and address is invalid.
-or-
filename is null or Empty.
-or-
The file does not exist.
-or- An error occurred while downloading data.  ");
        }
        catch (ArgumentNullException)
        {
            Console.Error.WriteLine("Error: The address parameter is null.");
        }
        catch (NotSupportedException)
        {
            Console.Error.WriteLine("The method has been called simultaneously on multiple threads.");
        }
    }
}
