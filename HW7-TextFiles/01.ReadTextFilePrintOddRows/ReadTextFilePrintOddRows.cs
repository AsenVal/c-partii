﻿using System;
using System.IO;

class ReadTextFilePrintOddRows
{
    static void Main()
    {
        string filename = "..//..//testfile.txt";
        StreamReader sr = new StreamReader(filename);
        string row = "";
        int i = 0;
        while(row != null)
        {
            row = sr.ReadLine();
            if (i % 2 == 0)
            {
                Console.WriteLine(row);
            }
            i++;
        }
        sr.Close();
        
    }
}
