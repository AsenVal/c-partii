﻿using System;
using System.IO;

class ConcatenatesTwoTextFilesIntoAnotherFile
{
    static void Main()
    {
        string filename = "..//..//testfile1.txt";
        string outputfile = "..//..//resultfile.txt";
        StreamReader sr = new StreamReader(filename);
        string str = sr.ReadToEnd();
        StreamWriter sw = new StreamWriter(outputfile, false);
        sw.WriteLine(str);
        sr.Close();
        sw.WriteLine("\n");
        filename = "..//..//testfile2.txt";
        sr = new StreamReader(filename);
        str = sr.ReadToEnd();
        sw.WriteLine(str);
        sr.Close();
        sw.Close();
    }
}
