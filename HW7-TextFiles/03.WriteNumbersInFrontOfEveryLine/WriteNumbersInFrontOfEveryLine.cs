﻿using System;
using System.IO;

class WriteNumbersInFrontOfEveryLine
{
    static void Main()
    {
        string filename = "..//..//testfile.txt";
        StreamReader sr = new StreamReader(filename);
        string row = "";
        string outputfile = "..//..//resultfile.txt";
        StreamWriter sw = new StreamWriter(outputfile, false);
        int i = 1;
        while (row != null)
        {
            row = sr.ReadLine();
            if (row != null)
            {
                sw.WriteLine(Convert.ToString(i + " " + row));
            }
            i++;
        }
        sr.Close();
        sw.Close();
    }
}
