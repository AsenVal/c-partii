﻿using System;
using System.IO;

class CompareTwoFilesAndReturnNumbersOfEqualLines
{
    static void Main()
    {
        string filename1 = "..//..//testfile1.txt";
        string filename2 = "..//..//testfile2.txt";
        StreamReader sr1 = new StreamReader(filename1);
        StreamReader sr2 = new StreamReader(filename2);
        string rowFirstFile = "";
        string rowSecondFile = "";
        int i = 1;
        while (rowFirstFile != null && rowSecondFile != null)
        {
            rowFirstFile = sr1.ReadLine();
            rowSecondFile = sr2.ReadLine();
            if (rowFirstFile == rowSecondFile)
            {
                Console.WriteLine("{0} line are same", i);
            }
            else
            {
                Console.WriteLine("{0} line are different", i);
            }
            i++;
        }
        sr1.Close(); 
        sr2.Close();
    }
}
