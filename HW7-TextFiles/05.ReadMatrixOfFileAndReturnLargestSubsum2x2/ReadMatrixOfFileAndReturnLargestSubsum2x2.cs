﻿using System;
using System.IO;

class ReadMatrixOfFileAndReturnLargestSubsum2x2
{
    static void Main()
    {
        //reading
        string filename = "..//..//Matrix.txt";
        StreamReader sr = new StreamReader(filename);
        string row = sr.ReadLine();
        int n = int.Parse(row);
        int[,] DoubleArray = new int[n, n];
        int j = 0;
        //string[] elements = new string[n];
        while (row != null)
        {
            row = sr.ReadLine();
            if (row != null)
            {
                string[] elements = row.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < n; i++)
                {

                    DoubleArray[j, i] = int.Parse(elements[i]);
                }
                j++;
            }
        }
        sr.Close();

        // Calculating
        int result = FindMaxSum(DoubleArray);

        // writing
        string outputfile = "..//..//resul.txt";
        StreamWriter sw = new StreamWriter(outputfile, false);
        sw.Write(result);
        sw.Close();
            
    }

    private static int FindMaxSum(int[,] DoubleArray)
    {
        int sum = 0;
        int largestSum = 0;
        int startrow = 0;
        int startcolumn = 0;
        for (int i = 0; i < DoubleArray.GetLength(0) - 1; i++)
        {
            for (int j = 0; j < DoubleArray.GetLength(1) - 1; j++)
            {
                for (int row = i; row < i + 2; row++)
                {
                    for (int col = j; col < j + 2; col++)
                    {
                        sum += DoubleArray[row, col];
                    }
                }
                if (sum > largestSum)
                {
                    largestSum = sum;
                    startrow = i;
                    startcolumn = j;
                }
                sum = 0;
            }
        }
        return largestSum;
    }
}
