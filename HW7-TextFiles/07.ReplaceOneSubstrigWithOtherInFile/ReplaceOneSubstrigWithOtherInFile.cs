﻿using System;
using System.IO;

class ReplaceOneSubstrigWithOtherInFile
{
    static void Main()
    {
        string filename = "..//..//testfile.txt";
        string outputfile = "..//..//resultfile.txt";
        StreamReader sr = new StreamReader(filename);
        StreamWriter sw = new StreamWriter(outputfile, false);
        string row = sr.ReadLine();
        while (row != null)
        {            
            sw.WriteLine(row.Replace("start","finish"));
            row = sr.ReadLine();
        }
        sr.Close();
        sw.Close();        
    }
}
