﻿using System;
using System.IO;
using System.Text.RegularExpressions;

class ReplaceOnlyWholeWordsInFile
{
    static void Main()
    {
        string filename = "..//..//testfile.txt";
        string outputfile = "..//..//resultfile.txt";
        StreamReader sr = new StreamReader(filename);
        StreamWriter sw = new StreamWriter(outputfile, false);
        string row = sr.ReadLine();
        while (row != null)
        {
            sw.WriteLine(Regex.Replace(row, @"\bstart\b", "finish"));
            row = sr.ReadLine();
        }
        sr.Close();
        sw.Close();
    }
}
