﻿using System;
using System.Collections.Generic;
using System.IO;

class ExtractFromXMLFileTextWithoutTags
{
    static void Main()
    {
        string filename = "..//..//testfile.txt";
        StreamReader sr = new StreamReader(filename);
        string row = sr.ReadLine();
        List<string> rows = new List<string>();
        int indexClosingTag = 0;
        int indexOpeningTag = 0;
        while (row != null)
        {

            while (indexClosingTag != -1 && indexOpeningTag != -1)
            {
                indexClosingTag = row.IndexOf('>', indexOpeningTag);
                indexOpeningTag = row.IndexOf('<', indexClosingTag);
                if (indexClosingTag != -1 && indexOpeningTag != -1)
                {
                    rows.Add(row.Substring(indexClosingTag + 1, indexOpeningTag - indexClosingTag - 1));
                }
            }
            row = sr.ReadLine();
            indexOpeningTag = 0;
            indexClosingTag = 0;

        }
        sr.Close();

        for (int i = 0; i < rows.Count; i++)
        {
            Console.WriteLine(rows[i]);
        }
        
    }
}
