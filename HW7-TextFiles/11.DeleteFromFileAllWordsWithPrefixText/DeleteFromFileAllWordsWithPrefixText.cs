﻿using System;
using System.IO;
using System.Text.RegularExpressions;

class DeleteFromFileAllWordsWithPrefixText
{
    static void Main()
    {
        string filename = "..//..//testfile.txt";
        StreamReader sr = new StreamReader(filename);
        string outputfile = "..//..//tempfile.txt";
        StreamWriter sw = new StreamWriter(outputfile, false);
        string row = sr.ReadLine();
        while (row != null)
        {
            /*int index1 = 0; 
            int index2 = 0;
            while(index1 != -1 && index2 != -1)
			{
                index1 = row.IndexOf(" test", index2);
                if (index1 != -1)
                {
                    index2 = row.IndexOf(" ", index1+1);
                    if (index2 != -1)
                    {
                        row = row.Substring(0, index1+1) + row.Substring(index2);
                        index2 = index1;
                    }
                }
			}
            sw.WriteLine(row);*/

            sw.WriteLine(Regex.Replace(row, "\\btest[A-Za-z0-9_]*\\b", String.Empty));
            //    //
            row = sr.ReadLine();
        }
        sr.Close();
        sw.Close();
        File.Delete(filename);
        File.Move(outputfile, filename);
        
    }
}
