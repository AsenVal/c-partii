﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

class FindListOfWordsAndCountEachOfThem
{
    static int BinSearch(List<Tuple<string, string>> array, string str)  ///// raboti za Item1
    {

        int MaxElement = array.Count - 1;
        int MinElement = 0;
        while (MaxElement >= MinElement)
        {
            int Midpoint = (MinElement + MaxElement) / 2;
            if (array[Midpoint].Item1.CompareTo(str) < 0)  // smeni Item1
            {
                MinElement = Midpoint + 1;
            }
            else if (array[Midpoint].Item1.CompareTo(str) > 0) // smeni Item1
            {
                MaxElement = Midpoint - 1;
            }
            else
            {
                return Midpoint;
            }
        }
        return -1;
    }

    static void Main()
    {
        string filename = "..//..//words.txt";
        try
        {
            StreamReader sr = new StreamReader(filename);
            // mnogomeren dictionary
            var listOfWords = new List<Tuple<string, int>>();  // Creating List of tupple
            string row = sr.ReadLine();
            while (row != null)
            {
                listOfWords.Add(new Tuple<string, int>(row.Replace(" ", String.Empty),0));  // adding elements to List of Tuple
                row = sr.ReadLine();
            }
            sr.Close();

            filename = "..//..//test.txt";
            sr = new StreamReader(filename);
            string outputfile = "..//..//result.txt";
            StreamWriter sw = new StreamWriter(outputfile, false);
            row = sr.ReadLine();
            while (row != null)
            {
                for (int i = 0; i < listOfWords.Count; i++)
                {
                    string RegularExpression = String.Concat("\\b", listOfWords[i].Item1, "\\b");
                    if (Regex.IsMatch(row, RegularExpression))
                    {
                        listOfWords[i] = new Tuple<string, int>(listOfWords[i].Item1, listOfWords[i].Item2 +1); // changing elements of the list
                    }
                }
                row = sr.ReadLine();
            }
            sr.Close();
            //PrintToConsole(listOfWords);
            //listOfWords.Sort((x,y) => (y.Item2).CompareTo(x.Item2)); // decreasing easy sort compare to second element

            // complex sort
            listOfWords.Sort((x,y) =>
                {
                    int result = (y.Item2).CompareTo(x.Item2); // decreasing compare to second element
                    if(result == 0)
                    {
                        result = (x.Item1).CompareTo(y.Item1); // increasing compare to first element
                    }
                    return result;
                });

            for (int i = 0; i < listOfWords.Count; i++)
            {
                sw.WriteLine("{0} - {1}", listOfWords[i].Item1, listOfWords[i].Item2);
            }

            sw.Close();

            //PrintToConsole(listOfWords);
        }
        catch (ArgumentNullException)
        {
            Console.WriteLine("Null Argument");
        }
        catch (ArgumentException)
        {
            Console.WriteLine("Wrong Argument");
        }
        catch (DirectoryNotFoundException)
        {
            Console.WriteLine("Directory to file not exist");
        }
        catch (FileNotFoundException)
        {
            Console.WriteLine("File not exist");
        }
        catch (IOException)
        {
            Console.WriteLine("File exception");
        }
    }

    private static void PrintToConsole(List<Tuple<string, int>> listOfWords)
    {
        for (int i = 0; i < listOfWords.Count; i++)
        {
            Console.WriteLine("{0} - {1}", listOfWords[i].Item1, listOfWords[i].Item2);
        }
        Console.WriteLine();
    }
}
