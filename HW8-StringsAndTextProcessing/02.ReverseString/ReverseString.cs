﻿using System;
using System.Text;

class ReverseString
{
    static void Main()
    {
        Console.WriteLine("Insert a string to reverse");
        string str = Console.ReadLine();
        char[] charArray = str.ToCharArray();
        Array.Reverse(charArray);
        Console.WriteLine(new string (charArray));
    }
}
