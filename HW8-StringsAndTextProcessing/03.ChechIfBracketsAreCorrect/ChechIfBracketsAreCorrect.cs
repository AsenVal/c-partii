﻿using System;
using System.Text;

class ChechIfBracketsAreCorrect
{
    static void Main()
    {
        StringBuilder sb = new StringBuilder();
        Console.WriteLine("Insert Equation to check it brackets");
        sb.Append(Console.ReadLine());
        int count = 0;
        for (int i = 0; i < sb.Length; i++)
        {
            if (sb[i] == '(')
                count++;
            if (sb[i] == ')')
                count--;
            if (count < 0)
                break;
        }
        if(count < 0)
            Console.WriteLine("Closing brackets \")\" are with {0} more than \"(\".",Math.Abs(count));
        else if (count > 0)
            Console.WriteLine("Opening brackets \"(\" are with {0} more than \")\".", count);
        else
            Console.WriteLine("Amount of opening and closing brackets is equal.");
    }
}
