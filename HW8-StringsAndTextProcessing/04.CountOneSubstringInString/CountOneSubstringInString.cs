﻿using System;
using System.Text.RegularExpressions;

class CountOneSubstringInString
{
    static void Main()
    {
        string str = "We are living in an yellow submarine. We don't have anything else. Inside the submarine is very tight. So we are drinking all the day. We will move out of it in 5 days.";
        Console.WriteLine("Insert string");
        //string str = Console.ReadLine();
        Console.WriteLine("Insert searching substring");
        string substr = Console.ReadLine();
        string pattern = "(?i)" + substr;
        Console.WriteLine("The result is {0}.",Regex.Matches(str, pattern).Count);

        //foreach (Match match in Regex.Matches(str, pattern))
        //    Console.WriteLine("{0} - found at index {1}.", match.Value, match.Index);  // dostapvane na indeksite i stoinostite na vsqko namirane
    }
}
