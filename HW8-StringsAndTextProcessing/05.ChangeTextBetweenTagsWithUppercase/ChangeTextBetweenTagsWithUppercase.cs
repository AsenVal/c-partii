﻿using System;
using System.Text.RegularExpressions;

class ChangeTextBetweenTagsWithUppercase
{
    static void Main()
    {
        string str = "We are living in a <upcase>yellow \nsubmarine</upcase>. We don't have \n<upcase>anything</upcase> else.";
        Console.WriteLine("Insert string with tags <upcase>...</upcase>");
        //string str = Console.ReadLine();
        Regex regex = new Regex (@"(<\s*upcase\s*>)(?<tag>[^<]*)(<\s*/upcase\s*>)");  // zameneno .*? sas [^<]*
        MatchCollection matches = regex.Matches(str);
        
        foreach (Match match in matches)
        {
            str = regex.Replace(str, match.Groups["tag"].Value.ToUpper(), 1);  
            //str = regex.Replace(str, Char.ToUpper(match.Groups["tag"].Value[0]) + match.Groups["tag"].Value.Substring(1), 1);   //Only first letter upper
            //Console.WriteLine("{0} - found at index {1}.", match.Value, match.Index);
        }
        //str = Regex.Replace(str, @"(<upcase>)(.*?)(</upcase>)", m => m.Groups[2].Value.ToUpper());
        //str = Regex.Replace(str, @"(<upcase>)(.*?)(</upcase>)", m => Char.ToUpper(m.Groups[2].Value[0]) + m.Groups[2].Value.Substring(1));
        Console.WriteLine(str);
    }
    
}
