﻿using System;

class ReadStringAndLenghtenTo20Sybmols
{
    static void Main()
    {
        Console.WriteLine("Insert a string small than 20 characters:");
        string str = Console.ReadLine();
        while (str.Length > 20)
        {
            Console.WriteLine("Your string is longer than 20 characters\nInsert a string small than 20 characters:");
            str = Console.ReadLine();
        }
        if (str.Length < 20)
        {
            str = String.Concat(str, new string('*', 20 - str.Length));
        }
        Console.WriteLine(str);
    }
}
