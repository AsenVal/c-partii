﻿using System;
using System.Text;

class EncodeDecodeString
{
    static void Main()
    {
        string str = "Some text for crypting and decrypting!";
        string cipher = "SecretCipher";

        Console.Write("Encrypted code:    ");
        string encrypted = Encrypt(str, cipher);
        Console.WriteLine(encrypted);

        Console.Write("Decrypted string   ");
        string deCrypt = DeCrypt(encrypted, cipher);
        Console.WriteLine(deCrypt);
    }
    static string Encrypt(string str, string cipher)
    {
        char[] strArray = str.ToCharArray();
        char[] cipherArray = new char[strArray.Length];

        StringBuilder encrypted = new StringBuilder();

        for (int i = 0, k = 0; i < cipherArray.Length; k++, i++)
        {
            if (!(k < cipher.Length))
            {
                k = 0;
            }
            cipherArray[i] = cipher[k];
            encrypted.Append((char)(strArray[i] ^ cipherArray[i]));
        }

        return encrypted.ToString();
    }
    static string DeCrypt(string str, string cipher)
    {
        return Encrypt(str, cipher).ToString();
    }
}
