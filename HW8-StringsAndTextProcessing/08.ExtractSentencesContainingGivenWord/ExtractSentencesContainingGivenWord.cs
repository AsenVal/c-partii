﻿using System;
using System.Text.RegularExpressions;

class ExtractSentencesContainingGivenWord
{
    static void Main()
    {
        string str = "We are living in an yellow submarine. We don't have \nanything else. Inside the submarine is very tight. \nSo we are drinking all the day. We will move out \nof it in 5 days.";
        //Console.WriteLine("Insert string");
        //string str = Console.ReadLine();
        Console.WriteLine("Insert searching substring");
        string substr = Console.ReadLine();
        string pattern = @"(?is)[A-Z][^\.]*\b" + substr + @"\b[^\.]*\.";
        MatchCollection matches = Regex.Matches(str,pattern);

        foreach (Match match in matches)
        {
            Console.WriteLine(Regex.Replace(match.Value,@"(?s)(.*?)(\n)(.*?)","$1$3"));
            //Console.WriteLine(match.Value);
        }
    }
}
