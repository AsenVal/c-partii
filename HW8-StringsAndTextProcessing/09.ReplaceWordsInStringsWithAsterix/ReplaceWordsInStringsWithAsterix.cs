﻿using System;
using System.Text.RegularExpressions;

class ReplaceWordsInStringsWithAsterix
{
    static void Main()
    {
        string str = "Microsoft announced its next generation PHP compiler today. It is based on .NET Framework 4.0 and is implemented as a dynamic language in CLR.";
        string[] words = new string[] { "PHP", "CLR", "Microsoft" };

        for (int i = 0; i < words.Length; i++)
        {
            string pattern = "\\b" + words[i] + "\\b";
            str = Regex.Replace(str, pattern, new string('*', words[i].Length));
        }
            Console.WriteLine(str);

    }
}
