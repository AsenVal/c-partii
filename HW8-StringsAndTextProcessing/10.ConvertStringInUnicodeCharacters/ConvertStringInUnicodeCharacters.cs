﻿using System;

class ConvertStringInUnicodeCharacters
{
    static void Main()
    {
        Console.WriteLine("Insert a string");
        string str = Console.ReadLine();
        char[] arr = str.ToCharArray();
        for (int i = 0; i < arr.Length; i++)
        {
            Console.Write("\\u{0:X4}", (int) arr[i]);
        }
    }
}
