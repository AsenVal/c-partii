﻿using System;

class PrintNumberInDifferentFormats
{
    static void Main()
    {
        Console.WriteLine("Insert a number");
        int n = int.Parse(Console.ReadLine());

        Console.WriteLine("{0,15:D} - Decimal Number", n);
        Console.WriteLine("{0,15:X4} - Hexadecimal Number", n);
        Console.WriteLine("{0,15:P} - Percentage:", n);
        Console.WriteLine("{0,15:E} - Scientific notation:", n);
    }
}
