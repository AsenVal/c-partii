﻿using System;
using System.Text.RegularExpressions;

class ParsesURLAddress
{
    static void Main()
    {
        string str = "URL http://www.devbg.org/forum/index.php, URL http://www.fefevbg.org/forum/index.php";
        //Console.WriteLine("Insert string");
        //string str = Console.ReadLine();
        string pattern = @"URL\s*(?<protocol>\w*?)\:[\\/]{2}(?<server>[^\\/]*)[\\/](?<resource>[^\s,;]*)";
        MatchCollection matches = Regex.Matches(str, pattern);

        foreach (Match match in matches)
        {
            Console.WriteLine("[protocol] = \"{0}\"",match.Groups["protocol"].Value);
            Console.WriteLine("[server] = \"{0}\"", match.Groups["server"].Value);
            Console.WriteLine("[resource] = \"{0}\"", match.Groups["resource"].Value);
            Console.WriteLine();
        }
    }
}
