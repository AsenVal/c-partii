﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

class ReverseWordsInSentence
{
    static void Main()
    {
        string str = "C# is not C++, not PHP and not Delphi!";
        Console.WriteLine(str);
        //Console.WriteLine("Insert string");
        //string str = Console.ReadLine();
        string pattern = @"\s+|\s*\.\s*|\s*,\s*|\s*!\s*|\s*\?\s*|\s*;\s*|\s*\:\s*";
        List<string> words = new List<string>();
        
        foreach (string s in Regex.Split(str, pattern))
        {
            if (!String.IsNullOrEmpty(s))
            {
                words.Add(s);
            }
        }
        words.Reverse();
        int i = 0;
        foreach (var match in Regex.Matches(str, pattern))
        {
            Console.Write(words[i] + match);
            i++;
        }
        Console.WriteLine();
    }
}
