﻿using System;
using System.Collections.Generic;

class TranslateWordFromDictionary
{
    static void Main()
    {
        var listOfWords = new List<Tuple<string, string>>();  // Creating List of tupple
        // adding elements to List of Tuple
        listOfWords.Add(new Tuple<string, string>(".NET", "platform for applications from Microsoft"));
        listOfWords.Add(new Tuple<string, string>("CLR", "managed execution environment for .NET"));
        listOfWords.Add(new Tuple<string, string>("namespace", "hierarchical organization of classes"));

        listOfWords.Sort((x, y) => (x.Item1).CompareTo(y.Item1));
        Console.WriteLine("Insert a word from [.NET/CLR/namespace] to be translated:");
        string word = Console.ReadLine();
        int index = BinSearch(listOfWords, word);
        if (index < 0)
        {
            Console.WriteLine("Element not exist at Dictionry");
        }
        else
        {
            Console.WriteLine(listOfWords[index].Item2);
        }
    }
    static int BinSearch(List<Tuple<string, string>> array, string str)  ///// raboti za Item1
    {
        
        int MaxElement = array.Count - 1;
        int MinElement = 0;
        while (MaxElement >= MinElement)
        {
            int Midpoint = (MinElement + MaxElement) / 2;
            if (array[Midpoint].Item1.CompareTo(str) < 0)  // smeni Item1
            {
                MinElement = Midpoint + 1;
            }
            else if (array[Midpoint].Item1.CompareTo(str) > 0) // smeni Item1
            {
                MaxElement = Midpoint - 1;
            }
            else
            {
                return Midpoint;
            }
        }
        return -1;
    }
}
