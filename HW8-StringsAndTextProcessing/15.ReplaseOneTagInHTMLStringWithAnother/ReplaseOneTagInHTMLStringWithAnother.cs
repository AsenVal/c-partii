﻿using System;
using System.Text.RegularExpressions;

class ReplaseOneTagInHTMLStringWithAnother
{
    static void Main()
    {
        string str = "<p>Please visit <a href=\"http://academy.telerik. com\">our site</a> to choose a training course. Also visit <a href=\"www.devbg.org\">our forum</a> to discuss the courses.</p>";
        Console.WriteLine(str);

        string pattern = @"<a\s*href\s*=\s*""(?<url>[^""]*)"">(?<text>[^<]*)<\s*/\s*a\s*>";
        Console.WriteLine(Regex.Replace(str, pattern, @"[URL=${url}]${text}[/URL]"));

    }
}
