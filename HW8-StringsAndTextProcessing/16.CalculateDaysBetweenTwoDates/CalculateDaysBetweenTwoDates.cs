﻿using System;
using System.Globalization;

class CalculateDaysBetweenTwoDates
{
    static void Main()
    {
        Console.WriteLine("Enter first date in DD.MM.YYYY format");
        DateTime firstDate = DateTime.ParseExact(Console.ReadLine(), new string[] { "d.M.yyyy", "dd.MM.yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        Console.WriteLine("Enter second date in DD.MM.YYYY format");
        DateTime secondDate = DateTime.ParseExact(Console.ReadLine(), new string[] { "d.M.yyyy", "dd.MM.yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        int days = (int)(secondDate - firstDate).TotalDays;
        Console.WriteLine(days);
    }
}
