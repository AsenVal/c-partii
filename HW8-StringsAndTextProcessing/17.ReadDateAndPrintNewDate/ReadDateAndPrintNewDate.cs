﻿using System;
using System.Globalization;

class ReadDateAndPrintNewDate
{
    static void Main()
    {
        Console.WriteLine("Enter date in day.month.year hour:minute:second  format");
        DateTime firstDate = DateTime.ParseExact(Console.ReadLine(), new string[] { "d.M.yyyy H:mm:ss", "dd.MM.yyyy H:mm:ss", "d.M.yyyy HH:mm:ss", "dd.MM.yyyy HH:mm:ss, d.M.yyyy HH:m:ss", "dd.MM.yyyy HH:m:ss" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
        firstDate = firstDate.AddHours(6.5);
        Console.WriteLine("{0}", firstDate.ToString("ddd dd.MM.yyyy HH:mm:ss", new CultureInfo("bg-BG")));
        //15.12.2012 19:48:25  
    }
}
