﻿using System;
using System.Text.RegularExpressions;

class ExtractEmailsAndSubstractMatchedFormat
{
    static void Main()
    {
        string str = "example() nakov@telerik.com. Try clicking the \"Run Match\",iv_todorov@yahoo.com button (or F5) to see what happens.";
        //Console.WriteLine("Insert string");
        //string str = Console.ReadLine();
        string pattern = @"(\w+)@(\w+)\.(\w+)";
        MatchCollection matches = Regex.Matches(str, pattern);

        foreach (Match match in matches)
        {
            Console.WriteLine(match.Value);
        }
    }
}
