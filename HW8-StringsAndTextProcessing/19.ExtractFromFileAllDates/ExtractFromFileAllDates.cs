﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

class ExtractFromFileAllDates
{
    static void Main()
    {
        string str = "Dates:12.5.2012 free 9.11.2001 11.12.2003, 12.7.41proba";
        //Console.WriteLine("Insert string");
        //string str = Console.ReadLine();
        string pattern = @"\d{1,2}\.\d{1,2}\.\d{4}";
        foreach (var match in Regex.Matches(str, pattern))
        {
            DateTime date = DateTime.ParseExact(match.ToString(), new string[] { "d.M.yyyy", "dd.MM.yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
            Console.WriteLine("{0}", date.ToString(CultureInfo.GetCultureInfo("en-CA").DateTimeFormat.ShortDatePattern));
        }
    }

}
