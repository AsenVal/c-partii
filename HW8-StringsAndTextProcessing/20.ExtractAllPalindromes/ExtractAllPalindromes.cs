﻿using System;

class ExtractAllPalindromes
{
    static void Main()
    {
        string str = "ABBA ffeerer efffefee, lamal ererfe exeff exe ";
        //Console.WriteLine("Insert string");
        //string str = Console.ReadLine();
        string[] arrayOfStrings = str.Split(new char[] { ' ', '.', ',', '!', '?' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < arrayOfStrings.Length; i++)
        {
            str = arrayOfStrings[i];
            bool IsPalindrom = false;
            for (int j = 0; j < str.Length / 2; j++)
            {
                if (str.Substring(j, 1) == str.Substring(str.Length - 1 - j, 1))
                {
                    IsPalindrom = true;
                }
                else
                {
                    IsPalindrom = false;
                    break;
                }
            }
            if (IsPalindrom == true && str.Length > 1)
                Console.WriteLine(str);
        }
    }
}
