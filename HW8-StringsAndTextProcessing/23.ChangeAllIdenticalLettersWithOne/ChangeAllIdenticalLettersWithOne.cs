﻿using System;
using System.Text.RegularExpressions;

class ChangeAllIdenticalLettersWithOne
{
    static void Main()
    {
        //string str = "aaaaabbbbbcdddeeeedssaa";
        //Console.WriteLine(str);
        Console.WriteLine("Insert string");
        string str = Console.ReadLine();
        string pattern = @"([a-zA-Z])\1+";
        
        Console.WriteLine(Regex.Replace(str,pattern,"$1"));
    }
}
