﻿using System;

class SortAlphabeticalListOfWords
{
    static void Main()
    {

        //string str = "Write a program that reads a string from the console and prints all different letters, in the string along with information how many times each letter is found.";
        Console.WriteLine("Insert a string");
        string str = Console.ReadLine();
        var words = str.Split(new char[] { ' ', '.', ',', '?', '!', ':', ';' },StringSplitOptions.RemoveEmptyEntries);
        Array.Sort(words);
        foreach (var word in words)
        {
            Console.WriteLine(word);
        }

    }
}
