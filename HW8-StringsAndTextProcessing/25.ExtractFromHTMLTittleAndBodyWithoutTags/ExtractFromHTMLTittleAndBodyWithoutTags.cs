﻿using System;
using System.Text.RegularExpressions;

class ExtractFromHTMLTittleAndBodyWithoutTags
{
    static void Main()
    {
        string str = @"<html>
  <head><title>News</title></head>
  <body><p><a href=""http://academy.telerik.com"">Telerik
    Academy</a>aims to provide free real-world practical
    training for young people who want to turn into
    skillful .NET software engineers.</p></body>
  </html>";
        string pattern = @"(?s)(?<=>)[^><]+?(?=</)";
        MatchCollection matches = Regex.Matches(str, pattern);

        foreach (Match match in matches)
        {
            Console.WriteLine(match.Value);
        }

    }
}
