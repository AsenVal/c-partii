﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

class BasicLanguage
{
    static void Main()
    {
        string str;
        StringBuilder sb = new StringBuilder();
        do
        {
            str = Console.ReadLine();
            sb.Append(str);
            sb.Append(Environment.NewLine);
        }
        while (!str.EndsWith("EXIT;"));

        //sb.Append("PRINT(Black and yellow, );");
        //sb.Append("FOR(0,1)PRINT(black and yellow, );");
        //sb.Append("PRINT(black and yellow...);");
        //b.Append("FOR ( 1 , 5 ) PRINT (ha) ;");
        //b.Append("FOR(2)FOR(2,3)PRINT(xi);PRINT(i);");
        //b.Append("EXIT;");

        str = sb.ToString();

        StringBuilder print = new StringBuilder();
        long broiCikli = 1;
        Match match = Regex.Match(str, @"(?s)(FOR\s*\(\s*(?<num>\d*)\s*\))|(FOR\s*\(\s*(?<num1>\-*\d*)\s*,\s*(?<num2>\-*\d*)\s*\))|(PRINT\s*\((?<text>[^\)].*?)\)\s*)|(PRINT\s*\(\)\s*)");
        while (match.Success)
        {
            if (match.Groups[1].Value != "" && match.Groups["num"].Value != "1")
            {
                broiCikli *= Convert.ToInt64(match.Groups["num"].Value);
            }
            if (match.Groups[2].Value != "")
            {
                broiCikli *= Convert.ToInt64(match.Groups["num2"].Value) - Convert.ToInt64(match.Groups["num1"].Value) + 1;
            }
            if (match.Groups[3].Value != "")
            {
                for (int i = 0; i < broiCikli; i++)
                {
                    print.Append(match.Groups["text"].Value);
                }
                broiCikli = 1;
            } 
            if (match.Groups[4].Value != "")
            {
                broiCikli = 1;
            }
            match = match.NextMatch();
        }
        Console.WriteLine(print.ToString());


        Console.WriteLine();
    }
}
