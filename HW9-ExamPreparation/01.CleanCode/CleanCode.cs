﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

class CleanCode
{
    static void Main()
    {
        int numberPositions = int.Parse(Console.ReadLine());
        string[] str = new string[numberPositions];
        for (int i = 0; i < numberPositions; i++)
        {
            str[i] = Console.ReadLine();
        }


        List<string> newstr = new List<string>();

        StringBuilder sb = new StringBuilder();
        
        bool isComment = false;
        bool isMComment = false;
        bool isKlyomba = false;
        bool isInString = false;
        string buffer = "";
        for (int j = 0; j < str.Length; j++)
        {
            sb.Append(str[j]);
            for (int i = 0; i < sb.Length; i++)
            {
                if (sb[i] == '\"' && i < sb.Length - 1)
                {
                    isInString = !isInString;
                    if (isKlyomba == true)
                    {
                        if (i == 0)
                        {
                            if (sb[i + 1] != '\"')
                                isKlyomba = false;
                        }
                        else
                            if (!(sb[i + 1] == '\"' && sb[i - 1] == '\"') && sb[i - 1] != '@')
                                isKlyomba = false;
                    }
                }

                if (sb[i] == '@' && !isComment && !isMComment)
                {
                    isKlyomba = true;
                }
                if (sb[i] == '/' && !isKlyomba && !isInString && !isComment && !isMComment)
                {
                    if (sb[i + 1] == '/')
                    {
                        isComment = true;
                    }
                    else if (sb[i + 1] == '*')
                    {
                        isMComment = true;
                    }
                }
                if (sb[i] == '*' && !isKlyomba && !isInString && isMComment && i < sb.Length - 1)
                    if (sb[i + 1] == '/')
                    {
                        isMComment = false;
                        sb.Remove(i, 2);
                        i = i--;
                    }
                if (isComment || isMComment)
                {
                    sb.Remove(i, 1);
                    i--;
                }
            }
            if (!isKlyomba)
            {
                isInString = false;
            }
            isComment = false;
            buffer += sb.ToString();
            if (isMComment == false)
            {
                if (sb.Length != 0 && !String.IsNullOrWhiteSpace(buffer))
                {
                    newstr.Add(buffer);
                }
                buffer = "";
            }
            sb.Clear();
        }

        for (int i = 0; i < newstr.Count; i++)
            {
            Console.WriteLine(newstr[i]);
        }
        //Console.WriteLine();
        /*for (int i = 0; i < numberPositions; i++)
        {
            

            string buffer;
            bool klyomba = false;
            if(Regex.IsMatch(str[i],@"//.*"))
            {
                Match match = Regex.Match(str[i], @"(.*)(//.*)");
                buffer = match.Groups[1].Value;
                MatchCollection matches = Regex.Matches(buffer, "\"");
                if (matches.Count % 2 == 0)
                {
                    str[i] = Regex.Replace(str[i], @"(.*)(//.*)", @"$1");
                }
                else
                {
                    if (Regex.IsMatch(buffer, "@\""))
                    {
                        klyomba = true;
                        if (Regex.IsMatch(buffer, @"@"".*(."".)"))
                        {
                            klyomba = false;
                        }
                        else if (Regex.IsMatch(str[i], @"@"".*(."".)"))
                        {
                            klyomba = false;
                        }
                    }
                }
            }
            
        }*/
        //str = Regex.Replace(str[i], pattern, String.Empty);

    }
}
