﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

class PHPVariables
{
    static void Main()
    {
        List<string> str = new List<string>();
        do
        {
            str.Add(Console.ReadLine());
        }
        while (str[str.Count - 1] != "?>");
//        List<string> str = new List<string>(){
//            "<?php",
//            "  $browser = /* This is $var1 in comments ;",
//            "  $var3 =*/ \"Some string \\$var4 with var //escaped.\"; $arr = array(); \"//  'variable': $var\"; // cpjkkjk $var",
//            "  $arr[$zero] = '\"code\"...{$valid_var}';",
//            "  var_dump($arr);",
//            "?>"
//        };
        List<string> output = new List<string>();
        bool isInString = false;
        bool isMComment = false;
        for (int i = 0; i < str.Count; i++)
        {
            Match match = Regex.Match(str[i], @"(.*?)\$(\w*)");
            while (match.Success)
            {
                string buffer = match.Groups[1].Value;
                MatchCollection matches = Regex.Matches(buffer, "['\"]");
                if (matches.Count % 2 == 0 ^ isInString)
                {
                    if (isInString)
                    {
                        if (!Regex.IsMatch(buffer, "['\"].*?#|/{2}")) // sled kavi4ka ako ima /*
                        {
                            if (!Regex.IsMatch(buffer, "/\\*") && isMComment == false) //  ako ima /*
                            {
                                output.Add(match.Groups[2].Value);
                            }
                            else
                            {
                                isMComment = true;
                            }
                            if (Regex.IsMatch(buffer, "\\*/"))
                            {
                                isMComment = false;
                            }
                        }
                    }
                    else
                    {
                        if (!Regex.IsMatch(buffer, "#|/{2}")) //  ako ima //
                        {
                            if (!Regex.IsMatch(buffer, "/\\*") && isMComment == false) //  ako ima /*
                            {
                                output.Add(match.Groups[2].Value);
                            }
                            else
                            {
                                isMComment = true;
                            }
                            if (Regex.IsMatch(buffer, "\\*/"))
                            {
                                isMComment = false;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    isInString = false;
                }
                else
                {
                    if (buffer.Length > 0)
                        if (buffer.Substring(buffer.Length - 1, 1) != "\\" && !isMComment)
                        {
                            output.Add(match.Groups[2].Value);
                        }
                    else if(buffer.Length > 1)
                    {
                        if (buffer.Substring(buffer.Length - 2, 2) == "\\\\" && !isMComment)
                        output.Add(match.Groups[2].Value);
                    }
                    isInString = true;
                    Match match1 = Regex.Match(buffer, "(.*?)\\*/");
                    if (match1.Success)
                    {
                        buffer = match1.Groups[1].Value;
                        matches = Regex.Matches(buffer, "['\"]");
                        if (matches.Count % 2 == 0)
                        {
                            isMComment = false;
                        }
                    }
                }
                match = match.NextMatch();
            }
            if (isMComment == false)
            {
                match = Regex.Match(str[i], "(.*?)/\\*");
                if (match.Success)
                {
                    string buffer = match.Groups[1].Value;
                    MatchCollection matches = Regex.Matches(buffer, "['\"]");
                    if (matches.Count % 2 == 0)
                    {
                        isMComment = true;
                    }
                } 
            }
            if (isMComment == true)
            {
                match = Regex.Match(str[i], "(.*?)\\*/");
                if (match.Success)
                {
                    string buffer = match.Groups[1].Value;
                    MatchCollection matches = Regex.Matches(buffer, "['\"]");
                    if (matches.Count % 2 == 0)
                    {
                        isMComment = false;
                    }
                }
            }
            isInString = false;

        }

        //                           ////////////////             sort by Char at ASCII
        output.Sort((x,y) => {
            int i = 0;
            do
            {
                if (i == Math.Min(x.Length, y.Length))
                {
                    if (x.Length == y.Length)
                    {
                        return 0;
                    }
                    else if (x.Length > y.Length)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }
                }
                if(Convert.ToChar(x.Substring(i,1)) > Convert.ToChar(y.Substring(i,1)))
                {
                    return 1;
                }
                else if (Convert.ToChar(x.Substring(i,1)) < Convert.ToChar(y.Substring(i,1)))
                {
                    return -1;
                }
                i++;
            }
            while(Convert.ToChar(x.Substring(i-1,1)) == Convert.ToChar(y.Substring(i-1,1)));
            return 0;
        });
        for (int i = 1; i < output.Count; i++)
        {
            if (output[i] == output[i - 1])
            {
                output.RemoveAt(i);
                i--;
            }
        }
        Console.WriteLine(output.Count);
        for (int i = 0; i < output.Count; i++)
        {
            Console.WriteLine(output[i]);
        }

    }
}
