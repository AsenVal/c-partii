﻿using System;
using System.Collections.Generic;

class Employees
{
    static void Main()
    {
        ////
        /*int numberPositions = 9;
        string[] inputpositions = new string[] 
        {
            "Trainee - 0",
            "Owner - 100",
            "CEO - 98",
            "Junior Developer - 30",
            "Unit Manager - 95",
            "Project Manager - 95",
            "Team Leader - 94",
            "Senior Developer - 50",
            "Developer - 40"
        };
        int numberEmployeers = 10;
        string[] inputEmployers = new string[] 
        {
            "Georgi Georgiev - Trainee",
            "Ademar Júnior - Unit Manager",
            "Dimitar Dimitrov - Owner",
            "Petar Atanasov - Project Manager",
            "Atanas Georgiev - Trainee",
            "Júnior Moraes - Trainee",
            "Ivan Bandalovski - Developer",
            "Apostol Popov - Developer",
            "Michel Platini - CEO",
            "Blagoy Makendzhiev - CEO"
        };*/
        int numberPositions = int.Parse(Console.ReadLine());
        string[] inputpositions = new string[numberPositions];
        for (int i = 0; i < numberPositions; i++)
        {
            inputpositions[i] = Console.ReadLine();
        }
        int numberEmployeers = int.Parse(Console.ReadLine());
        string[] inputEmployers = new string[numberEmployeers];
        for (int i = 0; i < numberEmployeers; i++)
        {
            inputEmployers[i] = Console.ReadLine();
        }
        //////
        string[] positions = new string[numberPositions];
        int[] koeficients = new int[numberPositions];
        for (int i = 0; i < numberPositions; i++)
        {
            string[] buffer = inputpositions[i].Split(new char[]{'-'}, StringSplitOptions.RemoveEmptyEntries);
            positions[i] = buffer[0].Trim();
            koeficients[i] = Convert.ToInt32(buffer[1]);
        }
        Array.Sort(positions, koeficients);
        var listOfWords = new List<Tuple<string, string, int>>();  // Creating List of tupple
        for (int i = 0; i < numberEmployeers; i++)
        {
            string[] buffer = inputEmployers[i].Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            int koef = Array.BinarySearch(positions, buffer[1].Trim());
            koef = koeficients[koef];
            buffer = buffer[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (buffer.Length == 2)
            {
                listOfWords.Add(new Tuple<string, string, int>(String.Join(" ", buffer, 0, buffer.Length - 1), buffer[buffer.Length - 1], koef));
            }
        }

        // complex sort
        listOfWords.Sort((x, y) =>
        {
            int result = (y.Item3).CompareTo(x.Item3); // decreasing compare to second element
            if (result == 0)
            {
                result = (x.Item2).CompareTo(y.Item2); // increasing compare to first element
                if (result == 0)
                {
                    result = (x.Item1).CompareTo(y.Item1); // increasing compare to first element
                }
            }
            return result;
        });

        for (int i = 0; i < listOfWords.Count; i++)
        {
            Console.WriteLine("{0} {1}", listOfWords[i].Item1, listOfWords[i].Item2);
        }
                        
    }
}
