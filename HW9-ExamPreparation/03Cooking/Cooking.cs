﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Cooking
{
    static void Main()
    {
        int n = int.Parse(Console.ReadLine());
        string[] Products = new string[n];
        for (int i = 0; i < n; i++)
        {
            Products[i] = Console.ReadLine(); ;
        }
        int m = int.Parse(Console.ReadLine());
        string[] Krisi = new string[m];
        for (int i = 0; i < m; i++)
        {
            Krisi[i] = Console.ReadLine(); ;
        }
        /*int n = 4;
        string[] Products = new string[]{            
            "2:fl ozs:Sugar",
            "1:cups:Sugar",
            "1.5:cups:Sugar",
            "1.006:ls:Old milK"
        };
        int m = 3;
        string[] Krisi = new string[]{
            "800:mls:old MILK",
            "1.5:cups:sugar",
            "1:fl ozs:sugar"
        };*/
        var units = new List<Tuple<string, double, string>>();
        units.Add(new Tuple<string, double, string>("tablespoons", 3, "teaspoons"));
        units.Add(new Tuple<string, double, string>("tbsps", 3, "tsps"));
        units.Add(new Tuple<string, double, string>("teaspoons", 0.3333, "tablespoons"));
        units.Add(new Tuple<string, double, string>("tsps", 0.3333, "tbsps"));
        units.Add(new Tuple<string, double, string>("gallons", 4, "quarts"));
        units.Add(new Tuple<string, double, string>("gals", 4, "quarts"));
        units.Add(new Tuple<string, double, string>("quarts", 0.25, "gallons"));
        units.Add(new Tuple<string, double, string>("quarts", 0.25, "gals"));
        units.Add(new Tuple<string, double, string>("liters", 1000, "milliliters"));
        units.Add(new Tuple<string, double, string>("ls", 1000, "mls"));
        units.Add(new Tuple<string, double, string>("milliliters", 0.001, "liters"));
        units.Add(new Tuple<string, double, string>("mls", 0.001, "ls"));
        units.Add(new Tuple<string, double, string>("pints", 2, "cups"));
        units.Add(new Tuple<string, double, string>("pts", 2, "cups"));
        units.Add(new Tuple<string, double, string>("cups", 0.001, "pints"));
        units.Add(new Tuple<string, double, string>("cups", 0.001, "pts"));
        units.Add(new Tuple<string, double, string>("cups", 8, "fluid ounces"));
        units.Add(new Tuple<string, double, string>("cups", 8, "fl ozs"));
        units.Add(new Tuple<string, double, string>("fluid ounces", 0.125, "cups"));
        units.Add(new Tuple<string, double, string>("fl ozs", 0.125, "cups"));
        units.Add(new Tuple<string, double, string>("quarts", 2, "pints"));
        units.Add(new Tuple<string, double, string>("qts", 2, "pts"));
        units.Add(new Tuple<string, double, string>("pints", 0.5, "quarts"));
        units.Add(new Tuple<string, double, string>("pts", 0.5, "qts"));
        units.Add(new Tuple<string, double, string>("teaspoons", 5, "milliliters"));
        units.Add(new Tuple<string, double, string>("teaspoons", 5, "mls"));
        units.Add(new Tuple<string, double, string>("milliliters", 0.2, "teaspoons"));
        units.Add(new Tuple<string, double, string>("mls", 0.2, "teaspoons"));
        units.Add(new Tuple<string, double, string>("cups", 48, "teaspoons"));
        units.Add(new Tuple<string, double, string>("cups", 48, "tsps"));
        units.Add(new Tuple<string, double, string>("teaspoons", 0.0208, "cups"));
        units.Add(new Tuple<string, double, string>("tsps", 0.0208, "cups"));
        units.Sort((x, y) =>
        {
            int result = (x.Item1).CompareTo(y.Item1); // increasing compare to first element
            if (result == 0)
            {
                result = (x.Item3).CompareTo(y.Item3); // increasing compare to third element
                if (result == 0)
                {
                    result = (x.Item2).CompareTo(y.Item2); // increasing compare to second element
                }
            }
            return result;
        });

        var listOfProducts = new List<Tuple<double, string, string>>();  // Creating List of tupple
        for (int i = n - 1; i >=0; i--)
        {
            string[] buffer = Products[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            listOfProducts.Add(new Tuple<double, string, string>(Convert.ToDouble(buffer[0]), buffer[1], buffer[2]));
        }
        listOfProducts.Sort((x, y) =>(x.Item3).CompareTo(y.Item3));
        double koef;
        for (int i = 1; i < listOfProducts.Count; i++)
        {
            if (listOfProducts[i].Item3.ToLower() == listOfProducts[i - 1].Item3.ToLower())
            {
                if (listOfProducts[i].Item2.ToLower() == listOfProducts[i - 1].Item2.ToLower())
                {
                    koef = listOfProducts[i].Item1 + listOfProducts[i - 1].Item1;
                    listOfProducts[i - 1] = new Tuple<double, string, string>(koef, listOfProducts[i - 1].Item2, listOfProducts[i - 1].Item3);
                    listOfProducts.RemoveAt(i);
                    i--;
                }
                else
                {
                    int index = BinSearch(units, listOfProducts[i].Item2, listOfProducts[i-1].Item2);
                    koef = listOfProducts[i].Item1 * units[index].Item2 + listOfProducts[i - 1].Item1;
                    listOfProducts[i - 1] = new Tuple<double, string, string>(koef, listOfProducts[i - 1].Item2, listOfProducts[i - 1].Item3);
                    listOfProducts.RemoveAt(i);
                    i--;
                }
            }
        }



        var listOfKrisi = new List<Tuple<double, string, string>>();
        for (int i = m - 1; i >= 0; i--)
        {
            string[] buffer = Krisi[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            int index = BinSearch(listOfProducts, buffer[2].ToLower());
            if (index != -1)
            {
                //listOfKrisi.Add(new Tuple<double, string, string>(Convert.ToDouble(buffer[0]), buffer[1], buffer[2]));
                if (buffer[1].ToLower() == listOfProducts[index].Item2.ToLower())
                {
                    listOfKrisi.Add(new Tuple<double, string, string>(Convert.ToDouble(buffer[0]), listOfProducts[index].Item2, listOfProducts[index].Item3));
                }
                else
                {
                    int index1 = BinSearch(units, buffer[1], listOfProducts[index].Item2);
                    koef = Convert.ToDouble(buffer[0]) * units[index1].Item2;
                    if (koef < listOfProducts[index].Item1)
                    {
                        listOfKrisi.Add(new Tuple<double, string, string>(koef, listOfProducts[index].Item2, listOfProducts[index].Item3));
                    }
                }

            }
        }

        listOfKrisi.Sort((x, y) => (x.Item3).CompareTo(y.Item3));
        for (int i = 1; i < listOfKrisi.Count; i++)
        {
            if (listOfKrisi[i].Item3.ToLower() == listOfKrisi[i - 1].Item3.ToLower())
            {
                if (listOfKrisi[i].Item2.ToLower() == listOfKrisi[i - 1].Item2.ToLower())
                {
                    koef = listOfKrisi[i].Item1 + listOfKrisi[i - 1].Item1;
                    listOfKrisi[i - 1] = new Tuple<double, string, string>(koef, listOfKrisi[i - 1].Item2, listOfKrisi[i - 1].Item3);
                    listOfKrisi.RemoveAt(i);
                    i--;
                }
                else
                {
                    int index = BinSearch(units, listOfKrisi[i].Item2, listOfKrisi[i - 1].Item2);
                    koef = listOfKrisi[i].Item1 * units[index].Item2 + listOfKrisi[i - 1].Item1;
                    listOfKrisi[i - 1] = new Tuple<double, string, string>(koef, listOfKrisi[i - 1].Item2, listOfKrisi[i - 1].Item3);
                    listOfKrisi.RemoveAt(i);
                    i--;
                }
            }
        }
        
        for (int i = 0; i < listOfProducts.Count; i++)
        {
            double pr = 0;
            int index = BinSearch(listOfKrisi, listOfProducts[i].Item3.ToLower());
            if (index !=-1)
            {
                pr = listOfProducts[i].Item1 - listOfKrisi[index].Item1;
            }
            else
            {
                pr = listOfProducts[i].Item1;
            }
            if(pr > 0)
            Console.WriteLine("{0:F2}:{1}:{2}",pr,listOfProducts[i].Item2,listOfProducts[i].Item3);
        }
        //Console.WriteLine();

    }

    static int BinSearch(List<Tuple<string, double, string>> array, string str, string look)  ///// raboti za Item1
    {

        int MaxElement = array.Count - 1;
        int MinElement = 0;
        while (MaxElement >= MinElement)
        {
            int Midpoint = (MinElement + MaxElement) / 2;
            if (array[Midpoint].Item1.CompareTo(str) < 0)  // smeni Item1
            {
                MinElement = Midpoint + 1;
            }
            else if (array[Midpoint].Item1.CompareTo(str) > 0) // smeni Item1
            {
                MaxElement = Midpoint - 1;
            }
            else
            {
                MinElement = Midpoint;
                MaxElement = Midpoint;
                while (array[MinElement].Item1.CompareTo(str) == 0 && MinElement > 0)
                {
                    MinElement--;
                }
                if (!(array[MinElement].Item1.CompareTo(str) == 0))
                {
                    MinElement++;
                }
                while (array[MaxElement].Item1.CompareTo(str) == 0 && MaxElement < array.Count - 1)
                {
                    MaxElement++;
                }
                if (!(array[MaxElement].Item1.CompareTo(str) == 0))
                {
                    MaxElement--;
                }
                while (MaxElement >= MinElement)
                {
                    Midpoint = (MinElement + MaxElement) / 2;
                    if (array[Midpoint].Item3.CompareTo(look) < 0)  // smeni Item1
                    {
                        MinElement = Midpoint + 1;
                    }
                    else if (array[Midpoint].Item3.CompareTo(look) > 0) // smeni Item1
                    {
                        MaxElement = Midpoint - 1;
                    }
                    else
                    {
                        return Midpoint;
                    }
                }
                return Midpoint;
            }
        }
        return -1;
    }
    static int BinSearch(List<Tuple<double, string, string>> array, string str)  ///// raboti za Item1
    {

        int MaxElement = array.Count - 1;
        int MinElement = 0;
        while (MaxElement >= MinElement)
        {
            int Midpoint = (MinElement + MaxElement) / 2;
            if (array[Midpoint].Item3.ToLower().CompareTo(str) < 0)  // smeni Item1
            {
                MinElement = Midpoint + 1;
            }
            else if (array[Midpoint].Item3.ToLower().CompareTo(str) > 0) // smeni Item1
            {
                MaxElement = Midpoint - 1;
            }
            else
            {
                return Midpoint;
            }
        }
        return -1;
    }
}
